`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 18.04.2024 07:21:08
// Design Name: 
// Module Name: reset_sinc
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module reset_sinc(
    input logic clk,reset,D,
    output logic Q
    );
    
    always_ff @(posedge clk)begin
        if(reset)
            Q<=0;
        else 
            Q<=D;
    end    
    
endmodule



