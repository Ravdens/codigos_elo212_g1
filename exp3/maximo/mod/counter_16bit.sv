`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 22.04.2024 23:25:55
// Design Name: 
// Module Name: counter_16bit
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module counter_Nbit
# (parameter N=16)
    (
    input logic clk , reset,
    output logic [N-1:0] count
    );
    always_ff @(posedge clk) 
    begin
        if(reset)
            count <= 0;
        else
            count <= count+1;       
    end
endmodule

