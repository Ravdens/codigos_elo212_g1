`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 19.04.2024 15:15:16
// Design Name: 
// Module Name: count_especial
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module count_especial(
    input logic clk, rst, ctrl,
    output logic [3:0] count
    );
    
    logic [3:0] count_aux, count_next;
    
    always_ff@(posedge clk)
        count <= count_next;
    
    always_comb
        if(ctrl == 1'b1)
            count_aux = count + 4'd2;
        else
            count_aux = count + 4'd1;
    
    always_comb
        case(rst)
            1'b1: count_next = 4'd0;
            1'b0: count_next = count_aux;
        endcase
        
     // assign count_aux (ctrl==2'b1)? count + 4'd2: count + 4'd1;
endmodule
