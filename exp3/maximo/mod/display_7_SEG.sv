`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 23.04.2024 11:23:21
// Design Name: 
// Module Name: display_7_SEG
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module display_7_SEG(
    input logic [3:0] anodo,
    input logic [6:0] display_tmp,
    output logic [27:0] display
    );
    
    always_comb begin
        case(anodo)
            14: display[6:0] = display_tmp[6:0];
            13: display[13:7] = display_tmp[6:0];
            11: display[20:14] = display_tmp[6:0];
            7: display[27:21] = display_tmp[6:0];
            default: display[27:0] = 15;
        endcase
    end
endmodule
