`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 19.04.2024 15:57:37
// Design Name: 
// Module Name: binary_decoder
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module binary_decoder(
    input logic [1:0] A,
    output logic [3:0] D
    );
    
    always_comb begin
        D[0] = A==0;
        D[1] = A==1;
        D[2] = A==2;
        D[3] = A==3;    //bool
    end
endmodule






