`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 23.04.2024 08:33:38
// Design Name: 
// Module Name: mux16_to_4
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module mux16_to_4(
    input logic [15:0] A,
    input logic [1:0] counter,
    output logic [3:0] B
    );
    
    always_comb begin
        case(counter)
            0: B[3:0] = A[3:0];
            1: B[3:0] = A[7:4];
            2: B[3:0] = A[11:8];
            3: B[3:0] = A[15:12];
        endcase
    end
endmodule
