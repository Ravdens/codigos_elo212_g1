`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 22.04.2024 23:27:55
// Design Name: 
// Module Name: test_bench_5_4
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module test_bench_5_4();
    logic clk_in,reset,reset2,clk_out;
    logic [1:0] counter2;
    logic [15:0] counter16;
    logic [3:0] B,C;
    logic [6:0] digit;
    logic [27:0] display;
    
    clock_divider2 #(
        .f_in(100),
        .f_out(25)      // frec_init/4 por cada display
    ) DUT1(
        .clk_in(clk_in),
        .reset(reset),
        .clk_out(clk_out)
    );
    
    counter_Nbit #(
        .N(2)
    ) DUT2(
        .clk(clk_in),
        .reset(reset),
        .count(counter2)
    );
    
    counter_Nbit #(
        .N(16)
    ) DUT3(
        .clk(clk_out),
        .reset(reset2),
        .count(counter16)
    );
    
    mux16_to_4 DUT4(
        .A(counter16),
        .counter(counter2),
        .B(B)
    );
    
    BCD_to_7_SEG DUT5(
        .BCD_in(B),
        .sevenSeg(digit)
    );
    
    binary_decoder DUT6(
        .A(counter2),
        .D(C)
    );
    
    display_7_SEG DUT7(
        .anodo(C),
        .display_tmp(digit),
        .display(display)
    );
    
    always #5 clk_in=~clk_in;
    
    initial begin
    clk_in=0;
    reset=1;
    #10
    reset=0;
    #10
    reset2=1;
    #10
    reset2=0;
    end
    
    
    
endmodule
