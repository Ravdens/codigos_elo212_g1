`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 22.04.2024 13:12:49
// Design Name: 
// Module Name: clock_divider_test_bench
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module clock_divider_test_bench();
    logic clk_in,reset,clk_out;
    
    clock_divider DUT(
        .clk_in(clk_in),
        .reset(reset),
        .clk_out(clk_out)
    );
    
    always #5 clk_in=~clk_in;
    
    initial begin
        clk_in = 0;
        reset = 1;
        #10
        reset = 0;
    end
    
endmodule
