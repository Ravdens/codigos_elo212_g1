`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 22.04.2024 20:39:15
// Design Name: 
// Module Name: clock_divider2_bench
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module clock_divider2_bench();
    logic clk_in,reset,
    clk_out1,clk_out2,clk_out3,clk_out4;
    
    clock_divider2 #(
        .f_in(100),
        .f_out(50)    
    )DUT1(
        .clk_in(clk_in),
        .reset(reset),
        .clk_out(clk_out1)
    );
    
    clock_divider2 #(
        .f_in(100),
        .f_out(30)    
    )DUT2(
        .clk_in(clk_in),
        .reset(reset),
        .clk_out(clk_out2)
    );
    
    clock_divider2 #(
        .f_in(100),
        .f_out(10)    
    )DUT3(
        .clk_in(clk_in),
        .reset(reset),
        .clk_out(clk_out3)
    );
    
    clock_divider2 #(
        .f_in(100),
        .f_out(1)    
    )DUT4(
        .clk_in(clk_in),
        .reset(reset),
        .clk_out(clk_out4)
    );
    
    always #5 clk_in=~clk_in;
    
    initial begin
        clk_in = 0;
        reset = 1;
        #10
        reset = 0;
    end
endmodule
