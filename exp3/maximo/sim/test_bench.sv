`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 18.04.2024 07:39:03
// Design Name: 
// Module Name: test_bench
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module test_bench();
    logic clk,reset,D,Q;
    
    reset_sinc DUT1(
        .clk(clk),
        .reset(reset),
        .D(D),
        .Q(Q)
    );
    reset_asinc DUT2(
        .clk(clk),
        .reset(reset),
        .D(D),
        .Q(Q)
    );
    
    always #5 clk=~clk;
    always #6 reset=~reset;
    
    initial begin
    clk=0;
    reset=1;
    end
endmodule
