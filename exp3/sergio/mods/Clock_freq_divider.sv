`timescale 1ns / 1ps

module Clock_freq_divider(
    input logic clock_100M, reset,
    output logic clock_out_50M , clock_out_30M , clock_out_10M, clock_out_1M
    );
    
    clock_divider_mod #(.F_IN(100),.F_OUT(50)) d50
    (
        .clk_in(clock_100M),
        .clk_out(clock_out_50M),
        .reset(reset)
    );
    
        clock_divider_mod #(.F_IN(100),.F_OUT(30)) d30
    (
        .clk_in(clock_100M),
        .clk_out(clock_out_30M),
        .reset(reset)
    );
    
        clock_divider_mod #(.F_IN(100),.F_OUT(10)) d10
    (
        .clk_in(clock_100M),
        .clk_out(clock_out_10M),
        .reset(reset)
    );
    
        clock_divider_mod #(.F_IN(100),.F_OUT(1)) d1
    (
        .clk_in(clock_100M),
        .clk_out(clock_out_1M),
        .reset(reset)
    );
endmodule
