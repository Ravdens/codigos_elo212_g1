`timescale 1ns / 1ps

module decoder2bit(
    // Entrada 2 bits => [1:0] (2 bits)
    input logic [1:0] X,
    // Salida 2^2 bits => [3:0] (4 bits)
    output logic [3:0] D
    );
    always_comb begin
        D[0] = X==0;
        D[1] = X==1;
        D[2] = X==2;
        D[3] = X==3;
    end
    
endmodule