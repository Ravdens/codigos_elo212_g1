`timescale 1ns / 1ps

module test_ff_sinc_asinc();
    logic x, clk, reset;
    logic y_sinc, y_asinc;
    
    ff_reset_sinc d1
    (
        .clk(clk),
        .reset(reset),
        .x(x),
        .y(y_sinc)
    );
    
    ff_reset_asinc d2
    (
        .clk(clk),
        .reset(reset),
        .x(x),
        .y(y_asinc)
    );
    
    always #1 clk = ~clk;
    
    initial 
    begin
        x= 0;
        clk = 0;
        reset = 1;
        #5
        reset = 0;
        #3
        x = 1;
        #1
        reset = 1;
        #2
        x=0;
        #2
        reset = 0;
        #3
        x= 1;
        #3
        x= 0;
        #5
        x = 1;
        #1
        reset = 1;
        #2
        x = 0;
        #5
        reset = 0;
    end 
endmodule
