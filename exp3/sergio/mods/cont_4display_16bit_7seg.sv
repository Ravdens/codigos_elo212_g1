`timescale 1ns / 1ps

module cont_4display_16bit_7seg(
    input logic clk, reset1,reset2,
    output logic [7:0] catodo_out, anodo_out 
    );
    
    logic clk_25M;
    clock_divider_mod #(.F_IN(100),.F_OUT(25)) d0 
    (
        .clk_in(clk),
        .clk_out(clk_25M),
        .reset(reset1)
    );
    logic [15:0]num_16;
    counter_Nbit #(.N(16)) d1
    (
        .clk(clk_25M),
        .reset(reset2),
        .count(num_16)
    );
    logic [3:0] num_4;
    always_comb begin
        case(cont_2)
            0 : num_4 = num_16[3:0];
            1 : num_4 = num_16[7:4];
            2 : num_4 = num_16[11:8];
            3 : num_4 = num_16[15:12];
        endcase
    end
    logic [6:0] num_out;
    BCD_to_sevenSeg d2 
    (
        .BCD_in(num_4),
        .sevenSeg(num_out)
    );
    logic [1:0] cont_2;
    counter_Nbit #(.N(2)) d3
    (
        .clk(clk),
        .reset(reset2),
        .count(cont_2)
    );
    logic [3:0] D;
    decoder2bit d4
    (
        .X(cont_2),
        .D(D)
    );
    logic [3:0] not_cont;
    assign not_cont = ~D;
    assign catodo_out = {num_out,1'b1};
    assign anodo_out = {1'b1,1'b1,1'b1,1'b1,~not_cont};
endmodule
