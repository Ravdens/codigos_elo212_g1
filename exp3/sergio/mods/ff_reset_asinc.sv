`timescale 1ns / 1ps


module ff_reset_asinc(
    input logic clk , reset, x,
    output logic y
    );
    always_ff @(clk or reset) 
    begin
        if(reset==1'b1)
            y <= 0;
        else
            y <= x;       
    end
endmodule
