`timescale 1ns / 1ps


module ff_reset_sinc(
    input logic clk , reset, x,
    output logic y
    );
    always_ff @(posedge clk) 
    begin
        if(reset==1'b1)
            y <= 0;
        else
            y <= x;       
    end
endmodule
