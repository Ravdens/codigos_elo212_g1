`timescale 1ns / 1ps

module count_especial(
        input logic ctrl, rst, clk,
        output logic [3:0] count
    );
    logic [3:0] count_aux , count_next;
    
    always_ff @(posedge clk)
    begin
        count <= count_next;
    end
    always_comb
    begin
        if(ctrl)
            count_aux = count + 4'd2;
        else
            count_aux = count + 4'd1;
        case(rst)
            1'b0: count_next = count_aux;
            1'b1: count_next = 4'd0;
         endcase
            
    end
    
endmodule
