`timescale 1ns / 1ps

module test_clock_divider();
    logic clk_i, reset;
    logic clk_o;
    
    clock_divider d0
    (
        .clk_in(clk_i),
        .clk_out(clk_o),
        .reset(reset)
    );
    
    always #5 clk_i = ~clk_i;
    
    initial begin
        clk_i = 0;
        reset = 1;
        #10
        reset = 0;
    end
endmodule
