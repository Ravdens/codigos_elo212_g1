`timescale 1ns / 1ps

module test_decoder2bit();
    logic [1:0] in;
    logic [3:0] out;
    
    decoder2bit d0(
        .X(in),
        .D(out)
    );
    
    initial begin
        for(int i =0;i<4;i=i+1) begin
            in = i;
            #5;
        end
        in = 0;
    end
endmodule
