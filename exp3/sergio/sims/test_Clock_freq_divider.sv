`timescale 1ns / 1ps

module test_Clock_freq_divider();
    logic clk, reset;
    logic clock_out_50M,clock_out_30M,clock_out_10M,clock_out_1M;
    
    Clock_freq_divider DUT
    (
        .clock_100M(clk),
        .clock_out_50M(clock_out_50M),
        .clock_out_30M(clock_out_30M),
        .clock_out_10M(clock_out_10M),
        .clock_out_1M(clock_out_1M),
        .reset(reset)
    );
    
    always #5 clk = ~clk;
    
    initial begin
        clk = 0;
        reset = 1;
        #10
        reset = 0;
    end
endmodule
