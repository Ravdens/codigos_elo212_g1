`timescale 1ns / 1ps

module test_cont_4display_16bit_7seg();
    
    logic clk, reset1,reset2;
    logic [7:0] anodo_out,catodo_out;
    
    cont_4display_16bit_7seg DUT
    (
        .clk(clk),
        .reset1(reset1),
        .reset2(reset2),
        .anodo_out(anodo_out),
        .catodo_out(catodo_out)
    );
    
    always #5 clk = ~clk;
    
    initial begin
        clk= 0;
        reset1 = 1;
        #10
        reset1 = 0;
        #10
        reset2 = 1;
        #10
        reset2 = 0;
    end
endmodule
