`timescale 1ns / 1ps

module test_decoder3bit();
    logic [2:0] in;
    logic [7:0] out;
    
    decoder3bit d0(
        .X(in),
        .D(out)
    );
    
    initial begin
        for(int i =0;i<8;i=i+1) begin
            in = i;
            #5;
        end
        in = 0;
    end
endmodule
