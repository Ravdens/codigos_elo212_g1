`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 06/12/2024 07:04:14 PM
// Design Name: 
// Module Name: test_S9_actividad3
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module test_S9_actividad3();
    logic clk, reset, Enter, Undo;
    logic [15:0] DataIn;
//    logic [15:0] ToDisplay;
    logic [7:0] Anodes;
    logic [6:0] Segments;
    logic [4:0] Flags;
    logic [2:0] Status;

    S9_actividad3 DUT(
        .clk(clk),
        .reset(reset),
        .Enter(Enter),
        .Undo(Undo),
        .DataIn(DataIn),
        .Segments(Segments),
        .Anodes(Anodes),
        .Flags(Flags),
        .Status(Status)
    );
    
    always #2 clk = ~clk;
    
    initial begin 
        clk=0;
        reset=0;
        Enter=0;
        Undo=0;
        DataIn=0;
        #1
        reset=1;
        #8
        reset=0;
        #8
        DataIn=5;
        #8
        Enter=1;
        #16
        Enter=0;
        #8
        DataIn=-8;
        #8
        Enter=1;
        #16
        Enter=0;
        #8
        DataIn=1;
        #8
        Enter=1;
        #16
        Enter=0;
        #32
        Undo =1;
        #16
        Undo =0;
        #8
        DataIn = 0;
        #8
        Enter=1;
        #16
        Enter=0;
        #8
        Undo=1;
        #16
        Undo=0;
        #16
        Undo=1;
        #16
        Undo=0;
        #16
        Undo=1;
        #16
        Undo=0;
        #16
        DataIn=-7;
        #8
        Enter = 1;
        #16
        Enter=0;
        #16
        DataIn=64;
        #8
        Enter = 1;
        #16
        Enter=0;
        #16
        DataIn=3;
        #8
        Enter = 1;
        #16
        Enter=0;
    end
endmodule
