`timescale 1ns / 1ps

module LevelToPulse(
    input logic clock, reset, L,
    output logic P
);
    
    enum logic[1:0] {S0,S1,S2} state, next_state;
    
    always_ff@(posedge clock) 
    begin
        if (reset)
            state <= S0;
        else
            state <= next_state;
    end
    
    always_comb 
    begin
        // next_state logic:
        next_state = S0;    // default next state
        case(state)
            S0 : if(L)
                    next_state = S1;
            S1 : if(L)
                    next_state = S2;
            S2 : if(L)
                    next_state = S2;
        endcase
        
        // output logic:
        case(state)
        S0 : P = 0;
        S1 : P = 1;
        S2 : P = 0;
        default : P = 0;
        endcase
    end
endmodule

