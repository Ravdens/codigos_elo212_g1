`timescale 1ns / 1ps

module DisplaySelector(
    input logic ToDisplaySel,
    input logic [15:0] Result, DataIn,
    output logic [15:0] ToDisplay
    );
    always_comb begin
        if (ToDisplaySel ==1)
            ToDisplay = DataIn;
        else
            ToDisplay = Result;
    end
endmodule
