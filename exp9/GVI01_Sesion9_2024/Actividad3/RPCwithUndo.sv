`timescale 1ns / 1ps

module RPCwithUndo(

    input logic clk, reset, Enter_pulse, Undo_pulse,
    output logic [2:0] Status,
    output logic loadOpA, loadOpB, loadOpCode, updateRes, ToDisplaySel
    );
    
    enum logic[2:0] {Entering_OpA, 
        Entering_OpB, 
        Entering_OpCode, 
        Load_OpA, Load_OpB, 
        Load_OpCode, 
        Show_Result} state, next_state;
    
    always_ff@(posedge clk) begin
        if(reset)
            state <= Entering_OpA;
        else
            state <= next_state;
    end
    
    // next_state logic
    always_comb begin
    //Definimos como grupo que el boton Undo no tendrá efecto durante los estados Load_OpX
    //Definimos como grupo que al apretar los botones Enter y Undo al mismo tiempo, los estados Load_OpX lo ignoraran y los estados de entrada se mantendrán igual.
        next_state = state;
        case(state)
            Entering_OpA: 
                if(Enter_pulse &! Undo_pulse)
                    next_state = Load_OpA;
            Entering_OpB:
                if(Enter_pulse & !Undo_pulse)
                    next_state = Load_OpB;
                else if(!Enter_pulse & Undo_pulse)
                    next_state = Entering_OpA;    
            Entering_OpCode:
                if(Enter_pulse & !Undo_pulse)
                    next_state = Load_OpCode;
                else if(!Enter_pulse & Undo_pulse)
                    next_state = Entering_OpB;
            Load_OpA: next_state = Entering_OpB;
            Load_OpB: next_state = Entering_OpCode;            
            Load_OpCode: next_state = Show_Result;
            Show_Result: 
                if(Enter_pulse & !Undo_pulse)
                    next_state = Entering_OpA;
                else if(!Enter_pulse & Undo_pulse)
                    next_state = Entering_OpCode;
//            default: next_state = Entering_OpA; // algun caso anomalo
        endcase
    end
    // output logic
    always_comb begin
        Status = 0;
        loadOpA = 0;
        loadOpB = 0;
        loadOpCode = 0;
        ToDisplaySel = 1;
        updateRes = 0;
        case(state)
            Entering_OpA: Status = 0;
            Entering_OpB: Status = 2;
            Entering_OpCode: Status = 4;
            Load_OpA:
                begin
                    Status = 1;
                    loadOpA = 1;
                end
            Load_OpB:
                begin
                    Status = 3;
                    loadOpB = 1;
                end        
            Load_OpCode:
                begin
                    Status = 5;
                    loadOpCode = 1;
                end
            Show_Result:
                begin
                    Status = 6;
                    ToDisplaySel = 0;
                    updateRes = 1;
                end
        endcase
    end
endmodule


