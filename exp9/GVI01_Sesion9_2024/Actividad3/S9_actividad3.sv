`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// University: Universidad Tecnica Federico Santa Maria
// Course: ELO212
// Students: Sergio Ehlen / Maximo Flores / Daniel Fernández
// 
// Create Date: 21/02/2024
// Design Name: Guia 9
// Module Name: S9_actividad3
// Project Name: 
// Target Devices: xc7a100tcsg324-1
// Tool Versions: Vivado 2021.1
// Description: Integra S9_actividad2 y conecta la salida a un driver para los display de 7 segmentos
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module S9_actividad3 (
    input   logic           clk,
    input   logic           reset,
    input   logic           Enter,
                            Undo,
    input   logic   [15:0]  DataIn,
    
    output  logic   [6:0]   Segments,
    output  logic   [7:0]   Anodes,
    output  logic   [4:0]   Flags,
    output  logic   [2:0]   Status
);

    logic [15:0] ToDisplay;
    
//    input logic clk, reset, Enter, Undo,
//    input logic [15:0] DataIn,
//    output logic [15:0] ToDisplay,
//    output logic [4:0] Flags,   // {N,Z,C,V,P}
//    output logic [2:0] Status
    S9_actividad2 Desarrollado_en_actividad_previa(
        .clk(clk),
        .reset(reset),
        .Enter(Enter),
        .Undo(Undo),
        .DataIn(DataIn),
        .ToDisplay(ToDisplay),
        .Flags(Flags),
        .Status(Status)
    );
    
//    input  logic        clock,
//    input  logic        reset,
//    input  logic [31:0] HEX_in,
//    output logic [6:0]  segments,
//    output logic [7:0]  anodes
    S4_actividad1 NumTo7Seg(
        .clock(clk),
        .reset(reset),
        .HEX_in({16'b0000_0000_0000_0000,ToDisplay}),
        .segments(Segments),
        .anodes(Anodes)
    );


endmodule

