`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// University: Universidad Tecnica Federico Santa Maria
// Course: ELO212
// Students: Sergio Ehlen / Maximo Flores / Daniel Fernández
// 
// Create Date: 21/02/2024
// Design Name: Guia 9
// Module Name: S9_actividad1
// Project Name: 
// Target Devices: xc7a100tcsg324-1
// Tool Versions: Vivado 2021.1
// Description: Funcionamiento calculadora polaca inversa usando ALU y FSM
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module S9_actividad1(
    input   logic           clk,
    input   logic           reset,
    input   logic           Enter,
    input   logic   [15:0]  DataIn,
    
    output  logic   [15:0]  ToDisplay,
    output  logic   [4:0]   Flags,
    output  logic   [2:0]   Status
    );
    
    //Señales internas Result_reg y Flags_comb se agregan pues no tienen nombre en el diagrama
    logic EnterPulse;
    logic LoadOpA,LoadOpB,LoadOpCode,updateRes,ToDisplaySel;
    //NOTA: Definimos como grupo ToDisplaySel=1 como mostrar DataIn, y 0 como mostrar Result
    logic [15:0] OpA,OpB,Result,Result_reg;
    logic [1:0] OpCode;
    logic [4:0] Flags_comb;
    
    //Este modulo solo es un conglomerado de otros modulos para
    //cumplir la definicion en el diagrama de la guía
    
    S8_actividad3 LevelToPulse(
        .clock(clk),
        .reset(reset),
        .L(Enter),
        .P(EnterPulse)
    );
    
    ReversePolishFSM ReversePolishFSM(
        .Enter_pulse(EnterPulse),
        .LoadOpA(LoadOpA),
        .LoadOpB(LoadOpB),
        .LoadOpCode(LoadOpCode),
        .updateRes(updateRes),
        .ToDisplaySel(ToDisplaySel),
        .Status(Status),
        .clk(clk),
        .reset(reset)
    );
    
    ALU_ref #(.M(16)) ALU(
        .A(OpA),
        .B(OpB),
        .OpCode(OpCode),
        .Result(Result),
        .Flags(Flags_comb)
    );

    DisplaySelector dispSel(
        .Result(Result_reg),
        .DataIn(DataIn),
        .ToDisplaySel(ToDisplaySel),
        .ToDisplay(ToDisplay)
    );
        
    regN_pipo #(.N(16)) rA(
        .clock(clk),
        .reset(reset),
        .load(LoadOpA),
        .in(DataIn),
        .out(OpA)
    );
    regN_pipo #(.N(16)) rB(
        .clock(clk),
        .reset(reset),
        .load(LoadOpB),
        .in(DataIn),
        .out(OpB)
    );
    regN_pipo #(.N(16))rR(
        .clock(clk),
        .reset(reset),
        .load(updateRes),
        .in(Result),
        .out(Result_reg)
    );
    regN_pipo #(.N(2)) rOp(
        .clock(clk),
        .reset(reset),
        .load(LoadOpCode),
        .in(DataIn[1:0]),
        .out(OpCode)
    );
    regN_pipo #(.N(5)) rLed(
        .clock(clk),
        .reset(reset),
        .load(updateRes),
        .in(Flags_comb),
        .out(Flags)
    );
endmodule

