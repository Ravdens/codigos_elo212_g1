`timescale 1ns / 1ps

module DisplaySelector(
// Este modulo es solo un mux pero se implemente asi porque sale en el diagrama
//y puede necesitar se editado despues
    input logic ToDisplaySel,
    input logic [15:0] Result, DataIn,
    output logic [15:0] ToDisplay
    );
    always_comb begin
        if (ToDisplaySel ==1)
            ToDisplay = DataIn;
        else
            ToDisplay = Result;
    end
endmodule

