`timescale 1ns / 1ps

module ReversePolishFSM(
    input logic clk, reset, Enter_pulse,
    output logic [2:0] Status,
    output logic LoadOpA, LoadOpB, LoadOpCode, updateRes, ToDisplaySel
    );
    
    //Se considera que como usamo 3 bits para el estado y ocupamos 7 estados, hay 1 estado posible no usado.
    enum logic[2:0] {Entering_OpA, Entering_OpB, Entering_OpCode, Load_OpA, Load_OpB, Load_OpCode, Show_Result} state, next_state;
    
    //Logica combinacional aislada de secuencial
    always_ff@(posedge clk) begin
        if(reset)
            state <= Entering_OpA;
        else
            state <= next_state;
    end
    
    // next_state logic
    always_comb begin
        //Se asume predeterminado se mantiene el estado
        next_state = state;
        case(state)
            Entering_OpA: 
                if(Enter_pulse)
                    next_state = Load_OpA;
            Entering_OpB:
                if(Enter_pulse)
                    next_state = Load_OpB;
            Entering_OpCode:
                if(Enter_pulse)
                    next_state = Load_OpCode;
            Load_OpA: next_state = Entering_OpB;
            Load_OpB: next_state = Entering_OpCode;            
            Load_OpCode: next_state = Show_Result;
            Show_Result: 
                if(Enter_pulse)
                    next_state = Entering_OpA;
                    
             //Consultar con el profe
             //Este default existe para un posible imposible
            default: next_state = Entering_OpA;   
                   
        endcase
    end
    // output logic
    always_comb begin
        //No se necesita default aca pues se define implicito al inicio
        Status= 0;
        LoadOpA = 0;
        LoadOpB = 0;
        LoadOpCode = 0;
        ToDisplaySel = 1;
        updateRes = 0;
        case(state)
            Entering_OpA: 
                begin
                    Status = 0;
                end
            Entering_OpB:
                begin
                    Status = 2;
                end
            Entering_OpCode:
                begin
                    Status = 4;
                end
            Load_OpA:
                begin
                    Status = 1;
                    LoadOpA = 1;
                end
            Load_OpB:
                begin
                    Status = 3;
                    LoadOpB = 1;
                end        
            Load_OpCode:
                begin
                    Status = 5;
                    LoadOpCode = 1;
                end
            Show_Result:
                begin
                    Status = 6;
                    ToDisplaySel = 0;
                    updateRes = 1;
                end
                
        endcase
    end
    
endmodule






