`timescale 1ns / 1ps

module regN_pipo#(parameter N=4)(
    input logic clock, reset, load,
    input logic [N-1:0] in,
    output logic[N-1:0] out
    );
    logic [N-1:0] next_out;
    always_ff@(posedge clock) begin
        out <= next_out;
    end
    always_comb begin
        if(reset)
            next_out = 0;
        else
            begin
            if(load)
                next_out=in;
            else
                next_out=out;
            end
    end
endmodule


