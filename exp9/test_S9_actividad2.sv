`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 06/12/2024 02:30:51 PM
// Design Name: 
// Module Name: test_S9_actividad2
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module test_S9_actividad2();
    logic clk, reset, Enter, Undo;
    logic [15:0] DataIn;
    logic [15:0] ToDisplay;
    logic [4:0] Flags;
    logic [2:0] Status;

    S9_actividad2 DUT(
        .clk(clk),
        .reset(reset),
        .Enter(Enter),
        .Undo(Undo),
        .DataIn(DataIn),
        .ToDisplay(ToDisplay),
        .Flags(Flags),
        .Status(Status)
    );
    
    always #2 clk = ~clk;
    
    initial begin 
        clk=0;
        reset=0;
        Enter=0;
        Undo=0;
        DataIn=0;
        #1
        reset=1;
        #4
        reset=0;
        #4
        DataIn=5;
        #4
        Enter=1;
        #8
        Enter=0;
        #4
        DataIn=-8;
        #4
        Enter=1;
        #8
        Enter=0;
        #4
        DataIn=1;
        #4
        Enter=1;
        #8
        Enter=0;
        #16
        Undo =1;
        #8
        Undo =0;
        #4
        DataIn = 0;
        #4
        Enter=1;
        #8
        Enter=0;
        #4
        Undo=1;
        #8
        Undo=0;
        #8
        Undo=1;
        #8
        Undo=0;
        #8
        Undo=1;
        #8
        Undo=0;
        #8
        DataIn=-7;
        #4
        Enter = 1;
        #8
        Enter=0;
        #8
        DataIn=64;
        #4
        Enter = 1;
        #8
        Enter=0;
        #8
        DataIn=3;
        #4
        Enter = 1;
        #8
        Enter=0;
    end
endmodule
