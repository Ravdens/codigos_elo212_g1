`timescale 1ns / 1ps
//    input   logic           clk,
//    input   logic           reset,
//    input   logic           Enter,
//    input   logic   [15:0]  DataIn,
    
//    output  logic   [15:0]  ToDisplay,
//    output  logic   [4:0]   Flags,
//    output  logic   [2:0]   Status


module test_S9_actividad1();
    logic clk, reset, Enter;
    logic [15:0] DataIn;
    logic [15:0] ToDisplay;
    logic [4:0] Flags;
    logic [2:0] Status;

    S9_actividad1 DUT(
        .clk(clk),
        .reset(reset),
        .Enter(Enter),
        .DataIn(DataIn),
        .ToDisplay(ToDisplay),
        .Flags(Flags),
        .Status(Status)
    );
    
    always #2 clk = ~clk;
    
    initial begin 
        clk=0;
        reset=0;
        Enter=0;
        DataIn=0;
        #1
        reset=1;
        #4
        reset=0;
        #4
        DataIn=5;
        #4
        Enter=1;
        #8
        Enter=0;
        #4
        DataIn=-8;
        #4
        Enter=1;
        #8
        Enter=0;
        #4
        DataIn=1;
        #4
        Enter=1;
        #8
        Enter=0;
        #16
        Enter=1;
        #8
        Enter=0;
    end

endmodule
