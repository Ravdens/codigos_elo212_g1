`timescale 1ns / 1ps

module S6_actividad3(
    input   logic           CLK100MHZ, CPU_RESETN, BTNC,
    input   logic [15:0]    SW,
    output  logic [4:0]     LED,
    output  logic           CA,CB,CC,CD,CE,CF,CG,
    output  logic [7:0]     AN 
    );
    logic reset, clock;
    logic [6:0] A,B,Result;
    logic [1:0] OpCode;
    logic [4:0] Flags;
    
    
    assign reset = ~ CPU_RESETN;
    assign A = SW[15:9];
    assign B = SW[8:2];
    assign OpCode = SW[1:0];
    
    S4_actividad3 #(.M(7)) ALU(
        .A(A),
        .B(B),
        .Result(Result),
        .OpCode(OpCode),
        .Flags(Flags)
    );
    
    clock_divider #(.COUNTER_MAX(100000)) clk_controler(
        .clk_in(CLK100MHZ),
        .reset(BTNC),
        .clk_out(clock)
    );
    
    S4_actividad1 display_driver(
    // se agregan 25 bits en 0 pues la ALU es de 7 bits pero HEX_in es una entrada de 32 bits
        .HEX_in({25'b0000000000000000000000000,Result}),
        .segments({CA,CB,CC,CD,CE,CF,CG}),
        .anodes(AN),
        .clock(clock),
        .reset(reset)
    );
    
    assign LED = Flags;
    
endmodule

