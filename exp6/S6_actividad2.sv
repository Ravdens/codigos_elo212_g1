`timescale 1ns / 1ps

module S6_actividad2(
    input   logic           CLK100MHZ, CPU_RESETN, BTNC,
    input   logic [15:0]    SW,
    output  logic [4:0]     LED,
    output  logic           CA,CB,CC,CD,CE,CF,CG,
    output  logic [7:0]     AN 
    );
    logic reset, clock_disp, clock_count, threshold;
    logic dec,enable,load;
    logic [3:0] count, load_ref_value;
    
    assign dec = SW[0];
    assign enable = SW[1];
    assign load = SW[2];
    assign load_ref_value = SW[15:12];
    assign LED ={3'b0000,threshold};
    assign reset = ~ CPU_RESETN;
    
    clock_divider #(.COUNTER_MAX(100000)) clk_disp_controler(
        .clk_in(CLK100MHZ),
        .reset(BTNC),
        .clk_out(clock_disp)
    );
    clock_divider #(.COUNTER_MAX(50000000)) clk_count_controler(
        .clk_in(CLK100MHZ),
        .reset(BTNC),
        .clk_out(clock_count)
    );
        
    S4_actividad2 #(.N(4)) counter(
        .clock(clock_count),
        .reset(reset),
        .dec(dec),
        .enable(enable),
        .load(load),
        .load_ref_value(load_ref_value),
        .counterN(count),
        .threshold(threshold)
    );
    
    S4_actividad1 display_driver(
    // se agregan 28 bits de 0 ya que el count es de 4 bits
        .HEX_in({28'b0000000000000000000000000000,count}),
        .segments({CA,CB,CC,CD,CE,CF,CG}),
        .anodes(AN),
        .clock(clock_disp),
        .reset(reset)
    );
    
endmodule

