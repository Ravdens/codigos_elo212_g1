`timescale 1ns / 1ps

module S6_actividad1(
    input   logic           CLK100MHZ, CPU_RESETN, BTNC,
    input   logic [15:0]    SW,
    output  logic [4:0]     LED,
    output  logic           CA,CB,CC,CD,CE,CF,CG,
    output  logic [7:0]     AN 
    );
    
    logic reset, clock;
    logic [15:0] num;
    
    assign reset = ~CPU_RESETN;
    assign num = SW;
    assign LED = 0;
    
    
//    clock_divider #(.COUNTER_MAX(2)) clk_controler(
    clock_divider #(.COUNTER_MAX(100000)) clk_controler(
        .clk_in(CLK100MHZ),
        .reset(BTNC),
        .clk_out(clock)
    );
    
    logic reset_delay;
    
//    delay_Nclk #(.N(4)) delayer(
//    delay_Nclk #(.N(4)) delayer(    
//        .clock(CLK100MHZ),
//        .reset(1'b0),
//        .shift_in(reset),
//        .enable(1'b1),
//        .shift_out(reset_delay)
//    );
    
    S4_actividad1 display_driver(
        .HEX_in({16'b0000000000000000,num}),
        .segments({CA,CB,CC,CD,CE,CF,CG}),
        .anodes(AN),
        .clock(clock),
        .reset(reset)
    );
    
endmodule

