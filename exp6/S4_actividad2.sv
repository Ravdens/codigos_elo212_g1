`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// University: Universidad Tecnica Federico Santa Maria
// Course: ELO212
// Students: Sergio Ehlen / Maximo Flores
// 
// Create Date: 21/04/2024
// Design Name: Guia 4
// Module Name: S4_actividad2
// Project Name: 
// Target Devices: xc7a100tcsg324-1
// Tool Versions: Vivado 2021.1
// Description: contador de N bits con parametros extra
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module S4_actividad2 #(parameter N = 32)(
    input  logic            clock, reset,
    input  logic            dec, enable, load,
    input  logic [N-1:0]    load_ref_value,
    output logic [N-1:0]    counterN,
    output logic            threshold
    );
    logic [N-1:0] next_val;
    logic [N-1:0] inc_val;

    // asignación comparativa simple para threshold.
    // De acuerdo a lo discutido con profesor en clase, queda a decisión de diseño la sincronisaciń de esta salida.
    // Como grupo decidimos que threshold se actualisa inmediatamente si load_ref_value cambia fuera de canto de reloj.
    assign threshold = counterN > load_ref_value;
    
    // asignación valor a flip flop, trabajaremos con next_val en la sección combinatoria.
    always_ff @(posedge clock) 
    begin
        counterN <= next_val;
    end
    
    always_comb
    begin
        //reset tiene mayor prioridad, asigna 0 o se evalua el resto de el circuito.
        if (reset)
            next_val = 4'h7;
        else
            //enable en 1 entonces se contea.
            if (enable == 1'b1)
            begin
                if (dec)
                begin
                    inc_val = counterN -1;
                    next_val = inc_val;
                end
                else
                begin
                    inc_val = counterN +1;
                    next_val = inc_val;
                end
            end
        
            //load prendido implica cargar valor. Solo llegamos a este
            //else if si enable está apagado.
            else if (load)
                next_val = load_ref_value;
        
            //No se carga ni aumenta valor, se mantiene el anterior.
            else
                next_val = counterN; 
                  
    end
endmodule


