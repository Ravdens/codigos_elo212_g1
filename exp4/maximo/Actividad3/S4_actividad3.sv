`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// University: Universidad Tecnica Federico Santa Maria
// Course: ELO212
// Students: 
// 
// Create Date: 
// Design Name: Guia 4
// Module Name: S4_actividad3
// Project Name: 
// Target Devices: xc7a100tcsg324-1
// Tool Versions: Vivado 2021.1
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module S4_actividad3 #(parameter M = 8)(
    input  logic [M-1:0]    A, B,
    input  logic [1:0]      OpCode,       
    output logic [M-1:0]    Result,
    output logic [4:0]      Flags       // {N,Z,C,V,P}
    );
    
    logic cout;
    logic [$clog2(M)-1:0] contar_unos;
    
    // Result
    always_comb begin
        case(OpCode)
        0: {cout,Result} = A-B;
        1: {cout,Result} = A+B;
        2: {cout,Result} = A|B;
        3: {cout,Result} = A&B;
        default: {cout,Result} = '0;
        endcase
    end
    always_comb begin
        logic [$clog2(M)+1:0] count;
        count = 0;
        for (int i = 0; i < M; i = i + 1) begin
            count = count + Result[i];
        end
        contar_unos = count;
    end
    // Flags
    assign Flags[4] = (Result[M-1]==1 && OpCode==0);              // N
    assign Flags[3] = Result==0;                                  // Z
    assign Flags[2] = (cout==1 && OpCode==1);                     // C
    assign Flags[1] = (cout ^ A[M-1] ^ B[M-1] && OpCode==0);      // V
    assign Flags[0] = contar_unos%2!=0;                           // P

endmodule