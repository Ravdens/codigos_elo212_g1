`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 25.04.2024 12:26:36
// Design Name: 
// Module Name: test_bench_4
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module test_bench_4();
    logic [3:0]    A, B;
    logic [1:0]      OpCode;       
    logic [3:0]    Result;
    logic [4:0]      Flags;       // {N,Z,C,V,P}
    
    S4_actividad3 #(
        .M(4)
    ) DUT1(
        .A(A),
        .B(B),
        .OpCode(OpCode),
        .Result(Result),
        .Flags(Flags)
    );
    
    initial begin
        A=8;
        B=10;
        OpCode = 0;
        #5
        OpCode = 1;
        #5
        OpCode = 2;
        #5
        OpCode = 3;
    end
endmodule
