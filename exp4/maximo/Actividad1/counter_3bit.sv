`timescale 1ns / 1ps


module counter_3bit(
    input logic clock , reset,
    output logic [2:0] count
    );
    always_ff @(posedge clock) 
    begin
        if(reset)
            count <= 0;
        else
            count <= count+1;       
    end

endmodule
