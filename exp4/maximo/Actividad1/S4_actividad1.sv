`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// University: Universidad Tecnica Federico Santa Maria
// Course: ELO212
// Students: Sergio Ehlen, M�ximo Flores
// 
// Create Date: 23-04-2024
// Design Name: Guia 4
// Module Name: S4_actividad1
// Project Name: 
// Target Devices: xc7a100tcsg324-1
// Tool Versions: Vivado 2021.1
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments: Se consider� el active low de los display
// de la Nexys, por lo que algunas salidas pueden estar 
// en su forma negada en caso de estar en active high.
// 
//////////////////////////////////////////////////////////////////////////////////


module S4_actividad1(
    input  logic        clock,
    input  logic        reset,
    input  logic [31:0] HEX_in,
    output logic [6:0]  segments,
    output logic [7:0]  anodes
    );
    // otras variables definidas en el dise�o
    logic [2:0] count;  // -> input:dec_3bit /output:counter_3bit
    logic [3:0] BCD_in; // -> input:BCD_to_SEVEN_SEG /output:(MUX8:1)
    
    //contador de 3-bit
    counter_3bit DUT0(
        .clock(clock),
        .reset(reset),
        .count(count)
    );
    //decodificador de 3-bit
    dec_3bit DUT1(
        .count(count),
        .anodes(anodes)
    );
    //BCD a 7 segmentos
    BCD_to_SEVEN_SEG DUT2(
        .BCD_in(BCD_in),
        .segments(segments)
    );
    //MUX8:1 -> HEX_in a BCD_in
    always_comb begin
        case(count)
            0: BCD_in = HEX_in[3:0];
            1: BCD_in = HEX_in[7:4];
            2: BCD_in = HEX_in[11:8];
            3: BCD_in = HEX_in[15:12];
            4: BCD_in = HEX_in[19:16];
            5: BCD_in = HEX_in[23:20];
            6: BCD_in = HEX_in[27:24];
            7: BCD_in = HEX_in[31:28];
            default: BCD_in = 0;    //cualquier otro caso
        endcase
    end
    
endmodule
