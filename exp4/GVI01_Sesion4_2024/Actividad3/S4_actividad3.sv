`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// University: Universidad Tecnica Federico Santa Maria
// Course: ELO212
// Students: Sergio Ehlen / Maximo Flores
// 
// Create Date: 21/04/2024
// Design Name: Guia 4
// Module Name: S4_actividad3
// Project Name: 
// Target Devices: xc7a100tcsg324-1
// Tool Versions: Vivado 2021.1
// Description: Alu básica a especificaciones de la guía
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module S4_actividad3 #(parameter M = 8)(
    input  logic [M-1:0]    A, B,
    input  logic [1:0]      OpCode,       
    output logic [M-1:0]    Result,
    output logic [4:0]      Flags
    );
    
    //Dividimos los flag en wires aparte para mejor legibilidad.
    logic N,Z,C,V,P;
    assign Flags = {N,Z,C,V,P};
    
    //Resultados de operaciones aritmeticas, consideran un bit extra para atrapar carry
    //obtenidos de operadores RTL.
    logic [M-1:0] S;
    logic [M-1:0] AmB,ApB,AyB,AoB;
    
    always_comb
    begin
    
        AmB = A - B;
        ApB = A + B;
        AyB = A & B;
        AoB = A | B;
    
    //Mux asigna resultado a operacion elejida.
    case(OpCode)
        2'b00 : S = AmB;
        2'b01 : S = ApB;
        2'b10 : S = AoB;
        2'b11 : S = AyB;
        default : S = 0;
    endcase
    
    //Para el bit de carry, se compara si hubo un exceso con A-B > A y A+B < A (corroborar)
    //Para las operaciones And y Or definimos C = 0
    case(OpCode)
        2'b00 : C = AmB[M-1:0]>A[M-1:0];
        2'b01 : C = ApB[M-1:0]<A[M-1:0];
        2'b10 : C = 0;
        2'b11 : C = 0; 
        default : C = 0;
    endcase

    //Para el bit de overflow, se compara el bit mas significativo dentro de la escala original
    //coherente con los ejemplos vistos en la guia (corroborar)
    //Para las operaciones And y Or definimos V = 0
    case(OpCode)
        2'b00 : V = ((~A[M-1])&(B[M-1])&(AmB[M-1]))|((A[M-1])&(~B[M-1])&(~AmB[M-1]));
        2'b01 : V = ((~A[M-1])&(~B[M-1])&(AmB[M-1]))|((A[M-1])&(B[M-1])&(~AmB[M-1]));
        2'b10 : V = 0;
        2'b11 : V = 0;
        default : V = 0;
    endcase          
    end
    
    //El bit de carry se entrega en los flag, el resultado es del mismo tamaño que las entradas
    assign Result = S[M-1:0];
    
    //Solamente se compara la parte de interes de la salida con 0
    assign Z = (Result==0);
    
    //El signo de un numero signado se ve en el bit mas significativo.
    assign N = Result[M-1];
    
    assign P = ^Result;
    
endmodule

