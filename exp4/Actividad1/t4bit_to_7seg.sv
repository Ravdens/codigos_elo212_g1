`timescale 1ns / 1ps

module t4bit_to_7seg(
    input logic [3:0] BCD_in,   //entrada BCD de 4-bit
    //se mantiene el nombre BCD_in por compatibilidad pero este modulo ahora acepta cualquier numero de 4 bits.
    output logic [6:0] sevenSeg     //salida de 7 segmentos(buses) conectadas al anodo
    );
    
    //Este modulo originalmente fue diseñado para logica directa, por lo que antes de salir se invierte la salida.
    logic [6:0] not_sevenSeg;
    assign sevenSeg = ~not_sevenSeg;    
    
    always_comb begin
        case(BCD_in) // ABCDEFG
            4'd0: not_sevenSeg = 7'b1111110;    // [1-9]
            4'd1: not_sevenSeg = 7'b0110000;
            4'd2: not_sevenSeg = 7'b1101101;
            4'd3: not_sevenSeg = 7'b1111001;
            4'd4: not_sevenSeg = 7'b0110011;
            4'd5: not_sevenSeg = 7'b1011011;
            4'd6: not_sevenSeg = 7'b1011111;
            4'd7: not_sevenSeg = 7'b1110000;
            4'd8: not_sevenSeg = 7'b1111111;
            4'd9: not_sevenSeg = 7'b1111011;
            4'hA: not_sevenSeg = 7'b1110111;
            4'hB: not_sevenSeg = 7'b0011111;
            4'hC: not_sevenSeg = 7'b1001110;
            4'hD: not_sevenSeg = 7'b0111101;
            4'hE: not_sevenSeg = 7'b1001111;
            4'hF: not_sevenSeg = 7'b1000111;
            default: not_sevenSeg = 7'b0000000;     //todos los segmentos OFF (max representacion de 1 digito)
        endcase
    end
    
endmodule

