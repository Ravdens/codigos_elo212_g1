`timescale 1ns / 1ps

module test_S4_actividad3();
    logic [3:0] A, B;
    logic [1:0] OpCode;
    logic [3:0] Result;
    logic [4:0] Flags;
    
    S4_actividad3 #(.M(4))DUT(
        .A(A),
        .B(B),
        .OpCode(OpCode),
        .Result(Result),
        .Flags(Flags)
    );
    
    initial
    begin
        //Suma
        OpCode = 2'b01;
        A = 4'b0101;
        B = 4'b0001;
        #10
        //Resta
        OpCode = 2'b00;
        #10
        //Suma que causa carry
        OpCode = 2'b01;
        A = 4'b1111;
        B = 4'b0001;
        #10
        //Resta que causa carry
        OpCode = 2'b00;
        A = 4'b0000;
        B = 4'b0001;
        #10
        OpCode = 2'b00;
        A = 4'b1111;
        B = 4'b1000;
        #10
        //Suma que causa overflow
        OpCode = 2'b01;
        A = 4'b0111;
        B = 4'b0001;
        #10
        //Resta que causa overflow
        OpCode = 2'b00;
        A = 4'b1000;
        B = 4'b0001;
        #10
        OpCode = 2'b00;
        A = 4'b1111;
        B = 4'b1000;
        #10
        OpCode = 0;
        A=0;
        B=0;
    
    end
endmodule

