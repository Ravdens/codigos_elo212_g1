`timescale 1ns / 1ps


module test_S4_actividad2 #(parameter N=5)();
    logic clock, reset, dec, enable, load;
    logic [N-1:0] load_ref_value;
    logic threshold;
    logic [N-1:0] counterN;
    
    
    S4_actividad2 #(.N(N)) DUT (
        .clock(clock),
        .reset(reset),
        .dec(dec),
        .enable(enable),
        .load(load),
        .load_ref_value(load_ref_value),
        .threshold(threshold),
        .counterN(counterN)
    );
    
    always #5 clock = ~clock;
    
    initial 
    begin
        clock = 0;
        reset = 1;
        enable = 1;
        load = 0;
        dec = 0;
        load_ref_value = 5;
        #9
        reset = 0;
        #10
        enable = 0;
        #10
        enable = 1;
        #20
        dec = 1;
        #30
        dec = 0;
        #20
        load = 1;
        #20
        enable = 0;
        #20
        enable = 1;
        load = 0;
        
    end
endmodule
