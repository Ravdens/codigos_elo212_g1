`timescale 1ns / 1ps


module test_S4_actividad1();
    logic clock, reset;
    logic [31:0] HEX_in;
    logic [6:0] segments;
    logic [7:0] anodes;
    
    S4_actividad1 DUT(
        .clock(clock),
        .reset(reset),
        .HEX_in(HEX_in),
        .segments(segments),
        .anodes(anodes)
    );
        
    always #5 clock = ~clock;

    initial 
    begin
        clock = 0;
        reset = 1;
        HEX_in = 32'h0000;
        #10
        reset = 0;
        #300
        HEX_in = 32'h12345678;
        #300
        HEX_in = 32'h89ABCDEF;
        #300
        HEX_in = 32'h0000;
        
    end
endmodule
