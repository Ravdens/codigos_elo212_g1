`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 06/09/2024 10:31:31 PM
// Design Name: 
// Module Name: test_S8_actividad2
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


`timescale 1ns / 1ps

module test_S8_actividad2();
    localparam N=16;
    logic             load_A,load_B,load_Op,updateRes, clk, reset;
    logic  [N-1:0]    data_in;
    logic [4:0]      LEDs;
    logic [6:0]      Segments;
    logic [7:0]      Anodes;
    
    S8_actividad2 #(.N(N)) DUT (
        .load_A(load_A),
        .load_B(load_B),
        .load_Op(load_Op),
        .updateRes(updateRes),
        .clk(clk),
        .reset(reset),
        .data_in(data_in),
        .LEDs(LEDs),
        .Segments(Segments),
        .Anodes(Anodes)
    );
    
    always #1 clk = ~clk;
    
    initial begin
        load_A=0;
        load_B=0;
        load_Op=0;
        updateRes=0;
        clk=0;
        reset=0;
        data_in=0;
        #2
        reset = 1;
        #2
        reset=0;
        #2
        updateRes=1;
        #2
        updateRes=0;
    
        #2
        data_in=-5;
        #2
        load_A=1;
        #2
        load_A=0;
        #2
        data_in=-8;
        #2
        load_B=1;
        #2
        load_B=0;
        #2
        data_in=1'd0;
        #2
        load_Op=1;
        #2
        load_Op=0;
        #2
        updateRes=1;
        #2
        updateRes=0;
        
        
        #2
        data_in=65530;
        #2
        load_A=1;
        #2
        load_A=0;
        #2
        data_in=30;
        #2
        load_B=1;
        #2
        load_B=0;
        #2
        data_in=1'd1;
        #2
        load_Op=1;
        #2
        load_Op=0;
        #2
        updateRes=1;
        #2
        updateRes=0;
    
    
        #2
        data_in=-32766;
        #2
        load_A=1;
        #2
        load_A=0;
        #2
        data_in=30;
        #2
        load_B=1;
        #2
        load_B=0;
        #2
        data_in=1'd0;
        #2
        load_Op=1;
        #2
        load_Op=0;
        #2
        updateRes=1;
        #2
        updateRes=0;
    
    
        #2
        data_in=16'b0010_1010_1111_1001;
        #2
        load_A=1;
        #2
        load_A=0;
        #2
        data_in=16'b0000_0000_0000_0110;
        #2
        load_B=1;
        #2
        load_B=0;
        #2
        data_in=1'd2;
        #2
        load_Op=1;
        #2
        load_Op=0;
        #2
        updateRes=1;
        #2
        updateRes=0;
    
        #2
        data_in=16'b0110_1110_1111_1001;
        #2
        load_A=1;
        #2
        load_A=0;
        #2
        data_in=16'b0100_1000_0000_0111;
        #2
        load_B=1;
        #2
        load_B=0;
        #2
        data_in=1'd3;
        #2
        load_Op=1;
        #2
        load_Op=0;
        #2
        updateRes=1;
        #2
        updateRes=0;
    
    end
    
    
endmodule

