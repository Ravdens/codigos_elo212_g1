`timescale 1ns / 1ps

module S8_actividad4(
    input logic clock, reset, Ta , Tb,
    output enum logic[1:0] {green, yellow, red} La, Lb
    );
    
    enum logic[1:0] {S0,S1,S2,S3} state, next_state;
    
    always_ff@(posedge clock) 
    begin
        if (reset)
            state <= S0;
        else
            state <= next_state;
    end
    
    always_comb 
    begin
        case(state)
            S0 : begin
                if(Ta)
                    next_state = S0;
                else
                    next_state = S1;
                end
            S1 : begin
                next_state = S2;
                end     
            S2 : begin
                if(Tb)
                    next_state = S2;
                else
                    next_state = S3;
                end    
            S3 : begin
                next_state = S0;
                end  
        endcase
        
        case(state)
            S0 : begin La = green ; Lb = red ; end
            S1 : begin La = yellow ; Lb = red ; end
            S2 : begin La = red ; Lb = green ; end
            S3 : begin La = red ; Lb = yellow ; end
        endcase
    end
endmodule
