`timescale 1ns / 1ps


module test_compare_ALUREG();
    localparam N=16;
    logic            load_A,load_B,load_Op,updateRes, clk, reset;
    logic [N-1:0]    data_in;
    logic [4:0]      LEDs1,LEDs3;
    logic [6:0]      Segments1,Segments3;
    logic [7:0]      Anodes1,Anodes3;
    
    S8_actividad2 #(.N(N)) DUT (
        .load_A(load_A),
        .load_B(load_B),
        .load_Op(load_Op),
        .updateRes(updateRes),
        .clk(clk),
        .reset(reset),
        .data_in(data_in),
        .LEDs(LEDs1),
        .Segments(Segments1),
        .Anodes(Anodes1)
    );
    
    G3_S8_actividad1 #(.N(N)) G3 (
        .load_A(load_A),
        .load_B(load_B),
        .load_Op(load_Op),
        .updateRes(updateRes),
        .clk(clk),
        .reset(reset),
        .data_in(data_in),
        .LEDs(LEDs3),
        .Segments(Segments3),
        .Anodes(Anodes3)
    );
    
    always #1 clk = ~clk;
    
    initial begin
        load_A=0;
        load_B=0;
        load_Op=0;
        updateRes=0;
        clk=0;
        reset=0;
        data_in=0;
        #2
        reset = 1;
        #2
        reset=0;
        #2
        updateRes=1;
        #2
        updateRes=0;
    
        #2
        data_in=-5;
        #2
        load_A=1;
        #2
        load_A=0;
        #2
        data_in=-8;
        #2
        load_B=1;
        #2
        load_B=0;
        #2
        data_in=1'd0;
        #2
        load_Op=1;
        #2
        load_Op=0;
        #2
        updateRes=1;
        #2
        updateRes=0;
        
        
        #2
        data_in=65530;
        #2
        load_A=1;
        #2
        load_A=0;
        #2
        data_in=30;
        #2
        load_B=1;
        #2
        load_B=0;
        #2
        data_in=1'd1;
        #2
        load_Op=1;
        #2
        load_Op=0;
        #2
        updateRes=1;
        #2
        updateRes=0;
    
    
        #2
        data_in=-32766;
        #2
        load_A=1;
        #2
        load_A=0;
        #2
        data_in=30;
        #2
        load_B=1;
        #2
        load_B=0;
        #2
        data_in=1'd0;
        #2
        load_Op=1;
        #2
        load_Op=0;
        #2
        updateRes=1;
        #2
        updateRes=0;
    
    
        #2
        data_in=16'b0010_1010_1111_1001;
        #2
        load_A=1;
        #2
        load_A=0;
        #2
        data_in=16'b0000_0000_0000_0110;
        #2
        load_B=1;
        #2
        load_B=0;
        #2
        data_in=1'd2;
        #2
        load_Op=1;
        #2
        load_Op=0;
        #2
        updateRes=1;
        #2
        updateRes=0;
    
        #2
        data_in=16'b0110_1110_1111_1001;
        #2
        load_A=1;
        #2
        load_A=0;
        #2
        data_in=16'b0100_1000_0000_0111;
        #2
        load_B=1;
        #2
        load_B=0;
        #2
        data_in=1'd3;
        #2
        load_Op=1;
        #2
        load_Op=0;
        #2
        updateRes=1;
        #2
        updateRes=0;
    
    end
endmodule
