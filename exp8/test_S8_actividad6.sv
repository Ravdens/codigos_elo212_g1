`timescale 1ns / 1ps
/*
	input 	logic clk,                 // base clock
	input 	logic rst,                 // global reset
	input 	logic PB,                  // raw asynchronous input from mechanical PB         
	output 	logic PB_pressed_status,   // clean and synchronized pulse for button pressed
	output  logic PB_pressed_pulse,    // high if button is pressed
	output  logic PB_released_pulse  
*/
module test_S8_actividad6();
    logic clk, rst, PB;
    logic FSM_PB_pressed_status, FSM_PB_pressed_pulse, FSM_PB_released_pulse;
    logic count_PB_pressed_status, count_PB_pressed_pulse, count_PB_released_pulse;

    PB_Debouncer_FSM #(.DELAY(15)) FSM(
        .clk(clk),
        .rst(rst),
        .PB(PB),
        .PB_pressed_status(FSM_PB_pressed_status),
        .PB_pressed_pulse(FSM_PB_pressed_pulse),
        .PB_released_pulse(FSM_PB_released_pulse)        
    );

    PB_Debouncer_counter #(.DELAY(15)) COUNT(
        .clk(clk),
        .rst(rst),
        .PB(PB),
        .PB_pressed_status(count_PB_pressed_status),
        .PB_pressed_pulse(count_PB_pressed_pulse),
        .PB_released_pulse(count_PB_released_pulse)        
    );

    always #1 clk = ~clk;
    
    initial begin 
        rst=1;
        clk=0;
        PB=0;
        #4
        rst=0;
        #20
        PB=1;
        #60
        PB=0;
        #10
        PB=1;
        #5
        PB=0;
        #10
        PB=1;
        #2
        PB=0;        
    end

endmodule
