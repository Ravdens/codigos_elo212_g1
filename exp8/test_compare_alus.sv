`timescale 1ns / 1ps

module test_compare_alus();
    logic [3:0] A,B;
    logic [1:0] OpCode;
    logic [3:0] Result_Grupo, Result_Ref;
    logic [4:0] Flags_Grupo, Flags_Ref;
    
    S4_actividad3 #(.M(4)) ALU_grupo(
        .A(A),
        .B(B),
        .OpCode(OpCode),
        .Result(Result_Grupo),
        .Flags(Flags_Grupo)
    );
    
    ALU_ref #(.M(4))ALU_ref(
        .A(A),
        .B(B),
        .OpCode(OpCode),
        .Result(Result_Ref),
        .Flags(Flags_Ref)
    );

    initial 
    begin
        A=0;
        B=0;
        OpCode=0;
        #5
        A=3;
        B=5;
        OpCode=0;
        #5
        A=5;
        B=4;
        OpCode=1;
        #5
        A=-2;
        B=-2;
        OpCode=1;
        #5
        A=-3;
        B=3;
        OpCode=0;
        #5
        A=4;
        B=-4;
        OpCode=0;
        #5
        A=4'b0101;
        B=4'b1010;
        OpCode=2;
        #5
        OpCode=3;
        #5
        A=4'b1011;
        B=4'b0111;
        OpCode=2;
        #5
        OpCode=3;
        #5
        A=0;
        B=0;
        OpCode=0;
    end
endmodule
