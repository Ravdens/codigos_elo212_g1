`timescale 1ns / 1ps

module test_S8_actividad3();
    logic reset, clock, L, P;
    
    S8_actividad3 DUT(
        .reset(reset),
        .clock(clock),
        .L(L),
        .P(P)
    );
    
    always #1 clock = ~clock;
    
    initial begin 
        reset=0;
        clock=0;
        L=0;
        #10
        reset=1;
        #5
        reset=0;
        #5
        L=1;
        #5
        L=0;
        #10
        L=1;
        #20
        L=0;
        #10
        L=1;
        #1
        L=0;
        #10
        L=1;
        #2
        L=0;
        
    end
    
    
endmodule
