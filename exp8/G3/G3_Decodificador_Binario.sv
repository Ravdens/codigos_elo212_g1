`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 19.04.2024 08:09:57
// Design Name: 
// Module Name: Decodificador_Binario
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

// N = 3
module Decodificador_Binario(
    input logic [1:0] a,
    output logic [7:0] d
    );
    
    always_comb
    begin
        case(a)
            2'b00 : d = ~8'b00000001;
            2'b01 : d = ~8'b00000010;
            2'b10 : d = ~8'b00000100;
            2'b11 : d = ~8'b00001000;
            //Para que los 4 m�s signiicaticos esten apagados
            //3'b100 : d = ~8'b00010000;
            //3'b101 : d = ~8'b00100000;
            //3'b110 : d = ~8'b01000000;
            //3'b111 : d = ~8'b10000000;
        endcase
    end                       
endmodule