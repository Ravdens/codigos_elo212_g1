`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 09.05.2024 18:40:10
// Design Name: 
// Module Name: G5_ALU_registrada
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module G3_S8_actividad1 #(parameter N = 16)(
    input logic [N-1:0] data_in,
    input logic load_A, load_B, load_Op, updateRes, clk,reset,
    output logic [6:0]  Segments,
    output logic [7:0]  Anodes,
	output logic [4:0]	LEDs
    );
    
    logic [N-1:0] A, B, Result, upResult;
    logic [4:0] Flags;
    logic [1:0] Op;
    
    
    always_ff @(posedge clk)begin
		if(reset)begin
			A = 0;
		end
		else begin
			case(load_A)
				1'b0 : A <= A;
				1'b1 : A <= data_in;
			endcase
		end
    end
    
    always_ff @(posedge clk)begin
		if(reset)begin
			B = 0;
		end
		else begin
			case(load_B)
				1'b0 : B <= B;
				1'b1 : B <= data_in;
			endcase
		end
    end
    
    always_ff @(posedge clk)begin
		if(reset)begin
			Op = 0;
		end
		else begin
			case(load_Op)
				1'b0 : Op <= Op;
				1'b1 : Op <= data_in[1:0];
			endcase
		end
    end
    
    G3_S4_actividad3 #(N) ALU(
        .A(A),
        .B(B),
        .OpCode(Op),
        .Result(Result),
        .Flags(Flags)
    );
    
    always_ff @(posedge clk)begin
		if(reset)begin
			upResult <= 0;
		end
		else begin
			case(updateRes)
				1'b0 : upResult <= upResult ;
				1'b1 : upResult <= Result;
			endcase
		end
    end
    
    always_ff @(posedge clk)begin
		if(reset) begin
			LEDs <= 0;
		end
		else begin
			case(updateRes)
				1'b0 : LEDs <= LEDs;
				1'b1 : LEDs <= Flags;
			endcase
		end
    end
    
    G3_S4_actividad1 Hex_7seg_driver(
		.clock(clk),
        .reset(reset),
        .HEX_in(upResult),
        .segments(Segments),
        .anodes(Anodes)
	);    
endmodule
