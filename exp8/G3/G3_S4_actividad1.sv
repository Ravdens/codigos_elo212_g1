`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// University: Universidad Tecnica Federico Santa Maria
// Course: ELO212
// Students: 
// 
// Create Date: 
// Design Name: Guia 4
// Module Name: S4_actividad1
// Project Name: 
// Target Devices: xc7a100tcsg324-1
// Tool Versions: Vivado 2021.1
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module G3_S4_actividad1(
        input  logic        clock,
        input  logic        reset,
        input  logic [15:0] HEX_in,
        output logic [6:0]  segments,
        output logic [7:0]  anodes
        );
        logic [3:0] H0, H1, H2, H3, Nibble;
        logic [1:0] select;
        
        Debuseador debuser(
            .HEX_in(HEX_in),
            .h0(H0),
            .h1(H1),
            .h2(H2),
            .h3(H3)             
        );
        //MUX seletor de cable HX
        always_comb begin
            case(select)
                2'b00 : Nibble = H0;
                2'b01 : Nibble = H1;
                2'b10 : Nibble = H2;
                2'b11 : Nibble = H3;
            endcase
        end
        
        BCD_7seg16 BCD_to_7seg(
            .BCD_in(Nibble),
            .sevenSeg(segments)
        );
        
        counterN#(2) selector(
            .clk(clock),
            .reset(reset),
            .count(select)
        );
        
        Decodificador_Binario deco(
            .a(select),
            .d(anodes)
        );
        
        
    endmodule
