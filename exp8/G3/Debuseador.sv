`timescale 1ns / 1ps

 module Debuseador(
    input logic [15:0] HEX_in,
    output logic [3:0]  h0, h1, h2, h3
);

    // Divisi�n de la entrada en conjuntos de 4 bits
    assign h0 = HEX_in[3:0];
    assign h1 = HEX_in[7:4];
    assign h2 = HEX_in[11:8];
    assign h3 = HEX_in[15:12];
endmodule
