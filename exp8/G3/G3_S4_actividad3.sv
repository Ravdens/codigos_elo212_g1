`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// University: Universidad Tecnica Federico Santa Maria
// Course: ELO212
// Students: 
// 
// Create Date: 
// Design Name: Guia 4
// Module Name: S4_actividad3
// Project Name: 
// Target Devices: xc7a100tcsg324-1
// Tool Versions: Vivado 2021.1
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module G3_S4_actividad3 #(parameter M = 8)(
    input  logic [M-1:0]    A, B,
    input  logic [1:0]      OpCode,       
    output logic [M-1:0]    Result,
    output logic [4:0]      Flags
    );
    logic V, C, Z, N, P;
    logic [M:0] salida;
    
    assign Result = salida[M-1:0];
    assign Flags = {N, Z, C, V, P};
    
    always_comb begin
        case(OpCode)
            2'b00 : begin
                    salida = A - B; 
                    V = (A[M-1] & ~B[M-1] & ~salida[M-1]) | (~A[M-1] & B[M-1] & salida[M-1]);
            end
            2'b01 : begin
                    salida = A + B;
                    V = (~A[M-1] & ~B[M-1] & salida[M-1]) | (A[M-1] & B[M-1] & ~salida[M-1]);
            end
            2'b10 : begin 
                salida = A | B;
                C = 1'b0;
                V = 1'b0;
            end 
            2'b11 : begin
                salida = A & B;
                C = 1'b0;
                V = 1'b0;
            end 
            default : salida = 'd0;
        endcase
        //Valor de V
        //V = (~A[M-1] & salida[M-1]) | (A[M-1] & ~salida[M-1]); //Al final el if es una tabla logica, que con carnot se deja así
		 // | salida > {M-1{'b1}} | A < B | // overflow de suma | resta | otros
		
        /*
        if(A[M-1] == 0 && B[M-1] == 0 && salida[M-1] == 1)begin//suma de positivos da negativo
            V = 1;
        end 
        else if(A[M-1] == 1 && B[M-1] == 1 && salida[M-1] == 0)begin//suma de negativos da positivo
            V = 1;
        end
        else if(A[M-1] == 0 && B[M-1] == 1 && salida[M-1] == 1)begin
            V = 1;
        end
        else if(A[M-1] == 1 && B[M-1] == 0 && salida[M-1] == 0)begin
            V = 1;
        end
        else begin
            V = 0;
        end
        */
        
        //Valor de C
        C = salida[M];//Salida en M esta el carry
      
        
        //Valor de Z
        Z = (Result == 0);//Si el resultado es 0, dara 1 o 0
        /*
        if(salida[M-1:0] == 0)begin
            Z = 1;
        end
        else begin
            Z = 0;
        end
        */
        
        //Valor de N
        N = Result[M-1];//== Result[M-1] El bit más significativo dará el signo
        /*
        if(salida[M-1] == 1)begin
            N = 1;
        end
        else begin
            N = 0;
        end
        */
        //Valor de P
        P = ^Result;// P es un XOR de los valores del esultado
    end
    
    
endmodule
