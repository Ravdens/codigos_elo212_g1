`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 14.04.2024 21:14:31
// Design Name: 
// Module Name: counterN
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module counterN #(parameter N = 4)(
    input logic clk, reset,
    output logic [N-1:0] count
);

    always_ff @(posedge clk or posedge reset) begin//Asincronico para que funcione la gustion
        if(reset)
            count <= 0; // Reinicia el contador con N bits en 0
        else
            count <= count + 1;
    end

endmodule

