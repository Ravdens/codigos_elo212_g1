`timescale 1ns / 1ps

module Two_Alus();
    
    S4_actividad3 #(.M(4)) ALU_grupo(
        .A(A),
        .B(B),
        .OpCode(OpCode),
        .Result(Result_Grupo),
        .Flags(Flags_Grupo)
    );
    
    ALU_ref #(.M(4))ALU_ref(
        .A(A),
        .B(B),
        .OpCode(OpCode),
        .Result(Result_Ref),
        .Flags(Flags_Ref)
    );
    
endmodule
