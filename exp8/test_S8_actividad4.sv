`timescale 1ns / 1ps

module test_S8_actividad4();
    logic reset, clock, Ta, Tb ;
    enum logic[1:0] {green, yellow, red} La, Lb;
    
    S8_actividad4 DUT(
        .reset(reset),
        .clock(clock),
        .Ta(Ta),
        .Tb(Tb),
        .La(La),
        .Lb(Lb)
    );
    
    always #1 clock = ~clock;
    
    initial begin 
        reset = 0;
        clock = 0;
        Ta = 0;
        Tb = 0;
        #5
        reset = 1;
        #5
        reset = 0;
        Ta = 1;
        Tb = 0;
        #20
        Ta = 0;
        #5
        Tb = 1;
        #20
        Tb = 0;        
    
    end
endmodule
