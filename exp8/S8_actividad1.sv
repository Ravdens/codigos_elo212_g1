`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: Universidad Tecnica Federico Santa Maria
// Engineer: Sergio, Maximo, Daniel
// 
// Create Date: 06/02/2024 04:31:49 PM
// Design Name: ALU_REG
// Module Name: S8_actividad1
// Project Name: Sesion 8
// Target Devices: NEXYS7
// Tool Versions: 6.9
// Description: Como una ALU, con REG
// 
// Dependencies: 
// 
// Revision: 07/06/2024
// Revision 0.01 - File Created
// Additional Comments: xd
// 
//////////////////////////////////////////////////////////////////////////////////


module S8_actividad1 #(parameter N=4)
(
    input logic             load_A,load_B,load_Op,updateRes, clk, reset,
    input logic  [N-1:0]    data_in,
    output logic [4:0]      LEDs,
    output logic [6:0]      Segments,
    output logic [7:0]      Anodes
    );
    logic [N-1:0] A,B,Result;
    logic [31:0] HEX_in;
    logic [4:0] Flags;
    logic [1:0] OpCode;
    
    S4_actividad3 #(.M(N)) alu(
        .A(A),
        .B(B),
        .OpCode(OpCode),
        .Result(Result),
        .Flags(Flags)
    );
    S4_actividad1 driver(
        .HEX_in(HEX_in),
        .segments(Segments),
        .anodes(Anodes),
        .clock(clk),
        .reset(reset)
    );

    regN_pipo #(.N(N)) rA(
        .clock(clk),
        .reset(reset),
        .load(load_A),
        .in(data_in),
        .out(A)
    );
    regN_pipo #(.N(N)) rB(
        .clock(clk),
        .reset(reset),
        .load(load_B),
        .in(data_in),
        .out(B)
    );
    regN_pipo #(.N(N))rR(
        .clock(clk),
        .reset(reset),
        .load(updateRes),
        .in(Result),
        .out(HEX_in)
    );
    regN_pipo #(.N(2)) rOp(
        .clock(clk),
        .reset(reset),
        .load(load_Op),
        .in(data_in[1:0]),
        .out(OpCode)
    );
    regN_pipo #(.N(N)) rLed(
        .clock(clk),
        .reset(reset),
        .load(updateRes),
        .in(Flags),
        .out(LEDs)
    );
    
endmodule

