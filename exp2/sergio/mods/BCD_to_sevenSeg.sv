`timescale 1ns / 1ps

module BCD_to_sevenSeg(
    input logic [3:0] BCD_in,   //entrada BCD de 4-bit
    output logic [6:0] sevenSeg     //salida de 7 segmentos(buses) conectadas al anodo
    );
    logic [6:0] not_sevenSeg;
    assign sevenSeg = ~not_sevenSeg;    
    always_comb begin
        case(BCD_in) // ABCDEFG
            4'd0: not_sevenSeg = 7'b1111110;    // [1-9]
            4'd1: not_sevenSeg = 7'b0110000;
            4'd2: not_sevenSeg = 7'b1101101;
            4'd3: not_sevenSeg = 7'b1111001;
            4'd4: not_sevenSeg = 7'b0110011;
            4'd5: not_sevenSeg = 7'b1011011;
            4'd6: not_sevenSeg = 7'b1011111;
            4'd7: not_sevenSeg = 7'b1110000;
            4'd8: not_sevenSeg = 7'b1111111;
            4'd9: not_sevenSeg = 7'b1111011;
            default: not_sevenSeg = 7'b0000000;     //todos los segmentos OFF (max representacion de 1 digito)
        endcase
    end
    
endmodule