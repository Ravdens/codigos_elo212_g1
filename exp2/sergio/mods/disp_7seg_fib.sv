`timescale 1ns / 1ps

module disp_7seg_fib(
    input logic clock, reset,
    output logic [6:0] num,
    output logic DP, on
    );
    
    logic [3:0] count;
    
    counter_4bit d1(
    .clk(clock),
    .reset(reset),
    .count(count)
    );
    
    BCD_to_sevenSeg d2(
        .BCD_in (count),
        .sevenSeg (num)
    );
    
    fib_rec d3(
        .BCD_in (count),
        .fib (DP)
    );
    
    assign on = 0;
endmodule