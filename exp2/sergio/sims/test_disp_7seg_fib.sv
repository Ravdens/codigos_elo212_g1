`timescale 1ns / 1ps

module test_disp_7seg_fib();
    logic clk, reset, DP, on;
    logic [6:0] num;
    
    disp_7seg_fib DUT
    (
    .clock(clk),
    .reset(reset),
    .num(num),
    .DP(DP),
    .on(on)
    );
    
    always #5 clk = ~clk;
    
    initial 
    begin
        clk = 0;
        reset = 1;
        #10
        reset = 0;
    end       
endmodule