`timescale 1ns / 1ps

module test_BC_to_sevenSeg();
    logic [3:0] BCD_in;
    logic [6:0] sevenSeg ;
    
    BCD_to_sevenSeg DUT(
        .BCD_in (BCD_in),
        .sevenSeg (sevenSeg)
    );
    initial begin
        for (int i=0; i<16; i=i+1) 
        begin
            BCD_in = i;
            #5;
        end
        
    end
endmodule