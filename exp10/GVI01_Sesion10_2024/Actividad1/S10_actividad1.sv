`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// University: Universidad Tecnica Federico Santa Maria
// Course: ELO212
// Students: Sergio Ehlen / Maximo Flores / Daniel Fernández
// 
// Create Date: 21/02/2024
// Design Name: Guia 10
// Module Name: S8_actividad1
// Project Name: 
// Target Devices: xc7a100tcsg324-1
// Tool Versions: Vivado 2021.1
// Description: Reemplaza los modulos LevelToPulse por Debouncers
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module S10_actividad1 #(
	parameter N_DEBOUNCER = 10
)(
    input   logic           clk,
    input   logic           resetN,
    input   logic           Enter,
                            Undo,
                            DisplayFormat,
    input   logic   [15:0]  DataIn,
    
    output  logic   [6:0]   Segments,
    output  logic   [7:0]   Anodes,
    output  logic   [4:0]   Flags,
    output  logic   [2:0]   Status
);
    logic reset;
    logic [15:0] ToDisplay;
    
    //Se invierte a la entrada la señal resetN para mantener el interior del módulo igual
    assign reset = ~resetN;
    

    S9_actividad2_Debouncer #(.N_DEBOUNCER(N_DEBOUNCER)) Calculadora(
        .clk(clk),
        .reset(reset),
        .Enter(Enter),
        .Undo(Undo),
        .DataIn(DataIn),
        .ToDisplay(ToDisplay),
        .Flags(Flags),
        .Status(Status)
    );
    
    S4_actividad1 Display_Driver(
        .clock(clk),
        .reset(reset),
        .HEX_in({16'b0000_0000_0000_0000,ToDisplay}),
        .segments(Segments),
        .anodes(Anodes)
    );


endmodule

