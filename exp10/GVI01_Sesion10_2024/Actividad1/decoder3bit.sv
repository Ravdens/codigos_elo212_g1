`timescale 1ns / 1ps

module decoder3bit(
    // Entrada 3 bits => [2:0] (3 bits)
    input logic [2:0] X,
    // Salida 2^3 bits => [7:0] (8 bits)
    output logic [7:0] D
    );
    always_comb begin
    //Esto funciona porque cada salida esta en alto solo cuando la entrada representa el valor
    //binario de el minterm correspondiente. Ej: D5 solo esta en alto cuando X = 3'b101 = 1'd5
        D[0] = X==0;
        D[1] = X==1;
        D[2] = X==2;
        D[3] = X==3;
        D[4] = X==4;
        D[5] = X==5;
        D[6] = X==6;
        D[7] = X==7;
    end
    
endmodule


