`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// University: Universidad Tecnica Federico Santa Maria
// Course: ELO212
// Students: Sergio Ehlen / Maximo Flores
// 
// Create Date: 21/02/2024
// Design Name: Guia 4
// Module Name: S4_actividad1
// Project Name: 
// Target Devices: xc7a100tcsg324-1
// Tool Versions: Vivado 2021.1
// Description: Recieves a 32 bit number and displays it in 8 
//              7 segment displays on the nexys dev board as hex values
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module S4_actividad1(
    input  logic        clock,
    input  logic        reset,
    input  logic [31:0] HEX_in,
    output logic [6:0]  segments,
    output logic [7:0]  anodes
    );
    
    logic [2:0] count;
    logic [3:0] disp_num;
    logic [7:0] not_anodes;
    
    //La salida de este contador se usa para multiplexion temporal entre 
    //numeros a mostrar.
    counter_Nbit #(.N(3)) d0 (
        .clk(clock),
        .reset(reset),
        .count(count)
    );
    
    //Nos dara señales con 1 alto a la vez, permite prender por turnos los display.
    decoder3bit d1 (
        .X(count),
        .D(not_anodes)
    );
    
    //Como se especifica en manual, los display necesitan anodo y catodo en 0 para prender, asique invertimos.
    assign anodes = ~not_anodes;
    
    //Multiplexor para mostrar un segmento de 4 bits a la vez. Recorre los segmentos de disp_num.
    always_comb
    begin
        case(count)
            3'b000 : disp_num = HEX_in[3:0] ;
            3'b001 : disp_num = HEX_in[7:4] ;
            3'b010 : disp_num = HEX_in[11:8] ;
            3'b011 : disp_num = HEX_in[15:12] ;
            3'b100 : disp_num = HEX_in[19:16] ;
            3'b101 : disp_num = HEX_in[23:20] ;
            3'b110 : disp_num = HEX_in[27:24] ;
            3'b111 : disp_num = HEX_in[31:28] ;
        endcase
    end
    
    //Para cada grupo de 4 bits a mostrar conseguimos su representacion en 7 segmentos para mostrar.
    //Este module fue diseñado con la tarjeta nexys en mente, asique las salidas usan logica inversa.
    t4bit_to_7seg d2 (
        .BCD_in(disp_num),
        .sevenSeg(segments)
    );
    
endmodule


