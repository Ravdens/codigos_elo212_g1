`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// University: Universidad Tecnica Federico Santa Maria
// Course: ELO212
// Students: Sergio Ehlen / Maximo Flores / Daniel Fernández
// 
// Create Date: 21/02/2024
// Design Name: Guia 9
// Module Name: S9_actividad2
// Project Name: 
// Target Devices: xc7a100tcsg324-1
// Tool Versions: Vivado 2021.1
// Description: Integra funcion Undo a el modulo anterior. Ahora usando debouncers
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

module S9_actividad2_Debouncer #(
    parameter N_DEBOUNCER = 10
)(
    input logic clk, reset, Enter, Undo,
    input logic [15:0] DataIn,
    output logic [15:0] ToDisplay,
    output logic [4:0] Flags,   // {N,Z,C,V,P}
    output logic [2:0] Status
);

// inner signals
logic Enter_pulse, Undo_pulse;
logic LoadOpA, LoadOpB, LoadOpCode, updateRes, ToDisplaySel;
//NOTA: Definimos como grupo ToDisplaySel=1 como mostrar DataIn, y 0 como mostrar Result
logic [15:0] OpA, OpB, Result, Result_reg;
logic [1:0] OpCode;
logic [4:0] Flags_comb;

//Se agregan los modulos debouncer del repositorio del ramo

PB_Debouncer_FSM #(.DELAY(N_DEBOUNCER)) Enter_Debouncer(
    .clk(clk),
    .rst(reset),
    .PB(Enter),
    .PB_pressed_pulse(Enter_pulse)
);

PB_Debouncer_FSM #(.DELAY(N_DEBOUNCER)) Undo_Debouncer(
    .clk(clk),
    .rst(reset),
    .PB(Undo),
    .PB_pressed_pulse(Undo_pulse)
);

RPCwithUndo rpc_with_undo(
    .clk(clk),
    .reset(reset),
    .Enter_pulse(Enter_pulse),
    .Undo_pulse(Undo_pulse),
    .Status(Status),
    .loadOpA(LoadOpA),
    .loadOpB(LoadOpB),
    .loadOpCode(LoadOpCode),
    .updateRes(updateRes),
    .ToDisplaySel(ToDisplaySel)
);

ALU_ref #(.M(16))alu(
    .A(OpA),
	.B(OpB),
	.OpCode(OpCode),
	.Result(Result),
	.Flags(Flags_comb)
);

DisplaySelector display_selector(
    .ToDisplaySel(ToDisplaySel),
    .Result(Result_reg),
    .DataIn(DataIn),
    .ToDisplay(ToDisplay)
);

regN_pipo #(.N(16)) rA(
    .clock(clk),
    .reset(reset),
    .load(LoadOpA),
    .in(DataIn),
    .out(OpA)
);

regN_pipo #(.N(16)) rB(
    .clock(clk),
    .reset(reset),
    .load(LoadOpB),
    .in(DataIn),
    .out(OpB)
);

regN_pipo #(.N(16)) rResult(
    .clock(clk),
    .reset(reset),
    .load(updateRes),
    .in(Result),
    .out(Result_reg)
);

regN_pipo #(.N(2)) rCode(
    .clock(clk),
    .reset(reset),
    .load(LoadOpCode),
    .in(DataIn[1:0]),
    .out(OpCode)
);

regN_pipo #(.N(5)) rLed(
    .clock(clk),
    .reset(reset),
    .load(updateRes),
    .in(Flags_comb),
    .out(Flags)
);
endmodule


