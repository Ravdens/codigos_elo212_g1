`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// University: Universidad Tecnica Federico Santa Maria
// Course: ELO212
// Students: Sergio Ehlen / Maximo Flores / Daniel Fernández
// 
// Create Date: 21/02/2024
// Design Name: Guia 10
// Module Name: S8_actividad2
// Project Name: 
// Target Devices: xc7a100tcsg324-1
// Tool Versions: Vivado 2021.1
// Description: Implementa el modulo Double-Dabble para el display
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module S10_actividad2 #(
	parameter N_DEBOUNCER = 10
)(
    input   logic           clk,
    input   logic           resetN,
    input   logic           Enter,
                            Undo,
                            DisplayFormat,
    input   logic   [15:0]  DataIn,
    
    output  logic   [6:0]   Segments,
    output  logic   [7:0]   Anodes,
    output  logic   [4:0]   Flags,
    output  logic   [2:0]   Status
);
    logic reset;
    logic [31:0] ToDisplay;
    
    //Se invierte a la entrada la señal resetN para mantener el interior del módulo igual
    assign reset = ~resetN;
    
    logic [15:0] pre_ToDisplay;

    S9_actividad2_Debouncer #(.N_DEBOUNCER(N_DEBOUNCER)) Calculadora(
        .clk(clk),
        .reset(reset),
        .Enter(Enter),
        .Undo(Undo),
        .DataIn(DataIn),
        .ToDisplay(pre_ToDisplay),
        .Flags(Flags),
        .Status(Status)
    );
    
    logic DF_pulse,DF_status;
    
    PB_Debouncer_FSM #(.DELAY(N_DEBOUNCER)) DisplayFormat_Debouncer(
        .clk(clk),
        .rst(reset),
        .PB(DisplayFormat),
        .PB_pressed_pulse(DF_pulse),
        .PB_pressed_status(DF_status)
    );
    

    logic [31:0] bcd_ToDisplay;
    
    unsigned_to_bcd Double_Dabble(
        .clk(clk),
        .reset(reset),
        .trigger(DF_pulse),
        .in({16'b0000_0000_0000_0000,pre_ToDisplay}),
        .bcd(bcd_ToDisplay)
    );
    
    
    always_comb begin 
        if(DF_status)
            ToDisplay = bcd_ToDisplay;
        else
            ToDisplay = {16'b0000_0000_0000_0000,pre_ToDisplay};
    end
    
    
    S4_actividad1 Display_Driver(
        .clock(clk),
        .reset(reset),
        .HEX_in(ToDisplay),
        .segments(Segments),
        .anodes(Anodes)
    );


endmodule

