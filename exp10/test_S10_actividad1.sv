`timescale 1ns / 1ps

module test_S10_actividad1 ();
    logic clk, reset, Enter, Undo, DisplayFormat;
    logic [15:0] DataIn;
//    logic [15:0] ToDisplay;
    logic [7:0] Anodes_5,Anodes_10,Anodes_30;
    logic [6:0] Segments_5,Segments_10,Segments_30;
    logic [4:0] Flags_5,Flags_10,Flags_30;
    logic [2:0] Status_5,Status_10,Status_30;

    S8_actividad1 #(.N_DEBOUNCER(5)) Delay_5(
        .clk(clk),
        .resetN(reset),
        .Enter(Enter),
        .Undo(Undo),
        .DisplayFormat(DisplayFormat),
        .DataIn(DataIn),
        .Segments(Segments_5),
        .Anodes(Anodes_5),
        .Flags(Flags_5),
        .Status(Status_5)
    );
    
        S8_actividad1 #(.N_DEBOUNCER(10)) Delay_10(
        .clk(clk),
        .resetN(reset),
        .Enter(Enter),
        .Undo(Undo),
        .DisplayFormat(DisplayFormat),
        .DataIn(DataIn),
        .Segments(Segments_10),
        .Anodes(Anodes_10),
        .Flags(Flags_10),
        .Status(Status_10)
    );
    
        S8_actividad1 #(.N_DEBOUNCER(30)) Delay_30(
        .clk(clk),
        .resetN(reset),
        .Enter(Enter),
        .Undo(Undo),
        .DisplayFormat(DisplayFormat),
        .DataIn(DataIn),
        .Segments(Segments_30),
        .Anodes(Anodes_30),
        .Flags(Flags_30),
        .Status(Status_30)
    );
    
    always #2 clk = ~clk;
    
    initial begin 
        clk=0;
        reset=1;
        Enter=0;
        Undo=0;
        DataIn=0;
        DisplayFormat=0;
        #1
        reset=0;
        #8
        reset=1;
        #8
        DataIn=5;
        #8
        Enter=1;
        #50
        Enter=0;
        #8
        DataIn=-8;
        #8
        Enter=1;
        #50
        Enter=0;
        #8
        DataIn=1;
        #8
        Enter=1;
        #50
        Enter=0;
        #32
        Undo =1;
        #50
        Undo =0;
        #8
        DataIn = 0;
        #8
        Enter=1;
        #50
        Enter=0;
        #8
        Undo=1;
        #50
        Undo=0;
        #50
        Undo=1;
        #50
        Undo=0;
        #50
        Undo=1;
        #50
        Undo=0;
        #16
        DataIn=-7;
        #8
        Enter = 1;
        #50
        Enter=0;
        #16
        DataIn=64;
        #8
        Enter = 1;
        #50
        Enter=0;
        #16
        DataIn=3;
        #8
        Enter = 1;
        #50
        Enter=0;
    end
endmodule
