`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 05.04.2024 15:56:38
// Design Name: 
// Module Name: fibbinario
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module fibbinario(
    input logic A,B,C,D,
    output logic F
    );
    
    assign F = (~A & ~C)|(~B & ~C)|(~B & ~D);
endmodule
