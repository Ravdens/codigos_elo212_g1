`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 05.04.2024 17:12:16
// Design Name: 
// Module Name: mod_3_10
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module mod_3_10(
input logic [3:0] A,B,C,D,E,
        logic [2:0] sel,
        output logic [3:0] out
    );
    
    always @ (A,B,C,D,sel) begin
        case(sel)
            3'b000 : out = A;
            3'b001 : out = B;
            3'b010 : out = C;
            3'b011 : out = D;
            3'b100 : out = E;
            default : out = 0;
        endcase
    end
endmodule
