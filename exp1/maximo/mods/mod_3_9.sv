`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 05.04.2024 17:01:13
// Design Name: 
// Module Name: mod_3_9
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module mod_3_9(
        input logic [3:0] A,B,C,D,
        logic [1:0] sel,
        output logic [3:0] out
    );
    
    always @ (A,B,C,D,sel) begin
        case(sel)
            2'b00 : out = A;
            2'b01 : out = B;
            2'b10 : out = C;
            2'b11 : out = D;
            default : out = 0;
        endcase
    end
    
endmodule
