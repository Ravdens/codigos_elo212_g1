`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 05.04.2024 17:31:34
// Design Name: 
// Module Name: sum3bit
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module sum3bit(
        input logic [2:0] A,B,
        input logic Cin,
        output logic Cout,
        output logic [2:0] S
    );
    
    logic [3:0] tmp;
    assign S = A + B + Cin;
    always_comb begin
        tmp = A + B + Cin;
        if(tmp[3]==1)
            Cout = 1;
        else
            Cout = 0;
    end
    
endmodule












