`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 04/05/2024 03:43:21 PM
// Design Name: 
// Module Name: fibbinario
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module fibbinario(
    input logic A,B,C,D,
    output logic f
    );
    
    logic notA, notB, notC, notD;
    assign notA = ~A;
    assign notB = ~B;
    assign notC = ~C;
    assign notD = ~D;
    
    logic and1, and2, and3;
    assign and1 = notA & notC;
    assign and2 = notB & notC;
    assign and3 = notD & notB;
    
    assign f = and1 | and2 | and3;
    
endmodule
