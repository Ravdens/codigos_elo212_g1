`timescale 1ns / 1ps

module sumadorNbit
# (parameter N=2)
    (
        input logic [N-1:0] A,B,
        input logic Cin,
        output logic Cout,
        output logic [N-1:0] S
    );
    
    logic [N:0] tmp;
    assign S = tmp[N-1:0];
    always_comb begin
        tmp = A + B + Cin;
        if(tmp[N]==1)
            Cout = 1;
        else
            Cout = 0;
    end
    
endmodule
