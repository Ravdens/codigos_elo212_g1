`timescale 1ns / 1ps

module sumador1bit(
        input logic A,B,
        input logic Cin,
        output logic Cout,
        output logic S
    );
    
    logic [1:0] tmp;
    //assign S = A + B + Cin;
    assign S = tmp[0];
    always_comb begin
        tmp = A + B + Cin;
        if(tmp[1]==1)
            Cout = 1;
        else
            Cout = 0;
    end
    
endmodule
