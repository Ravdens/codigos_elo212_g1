`timescale 1ns / 1ps

module mux_3b_4ent(
    input logic A[3:0],B[3:0],C[3:0],D[3:0],sel,
    output logic out[3:0]
    );
    
    always_comb
    begin
        case(sel)
        2'b00 :  out = A;
        2'b01 :  out = B;
        2'b10 :  out = C;
        2'b11 :  out = D;
        //default : out = 0;
        endcase
    end
endmodule
