

module test_sum2bit();
    logic [1:0] A,B,S;
    logic Cin,Cout;
    
    sumadorNbit DUT(
        .A (A),
        .B (B),
        .S (S),
        .Cin (Cin),
        .Cout (Cout)
    );
    
    initial 
    begin
    //Inicial
        A=2'd0;
        B=2'd0;
        Cin=1'b0;
        #3
        
        A=2'd1;
        #3
        A=2'd2;
        #3
        A=2'd3;
        #3
        
        B=2'd0;
        A=2'd0;
        #3
        A=2'd1;
        #3
        A=2'd2;
        #3
        A=2'd3;
        #3
        
        B=2'd1;
        A=2'd0;
        #3
        A=2'd1;
        #3
        A=2'd2;
        #3
        A=2'd3;
        #3
        
        B=2'd2;
        A=2'd0;
        #3
        A=2'd1;
        #3
        A=2'd2;
        #3
        A=2'd3;
        #3
        
        B=2'd3;
        A=2'd0;
        #3
        A=2'd1;
        #3
        A=2'd2;
        #3
        A=2'd3;
        #3
        
        Cin=1'b1;
        B=2'd0;
        
        A=2'd0;
        #3
        A=2'd1;
        #3
        A=2'd2;
        #3
        A=2'd3;
        #3
        
        B=2'd1;
        A=2'd0;
        #3
        A=2'd1;
        #3
        A=2'd2;
        #3
        A=2'd3;
        #3
        
        B=2'd2;
        A=2'd0;
        #3
        A=2'd1;
        #3
        A=2'd2;
        #3
        A=2'd3;
        #3
        
        B=2'd3;
        A=2'd0;
        #3
        A=2'd1;
        #3
        A=2'd2;
        #3
        A=2'd3;
        #3
        
        //Finale
        A = 2'd0;
        B = 2'd0;
        Cin = 1'b0;
        
    end
endmodule
