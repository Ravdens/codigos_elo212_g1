`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 04/05/2024 03:55:49 PM
// Design Name: 
// Module Name: test_4bit
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module test_4bit();
    logic A,B,C,D;
    logic F;
    
    fibbinario DUT(
        .A (A),
        .B (B),
        .C (C),
        .D (D),
        .f (F)
    );
    initial begin

    // C = 0 D = 0
        A = 1'b0;
        B = 1'b0;
        C = 1'b0;
        D = 1'b0;
        #3
        A = 1'b1;
        B = 1'b0;
        C = 1'b0;
        D = 1'b0;
        #3
        A = 1'b0;
        B = 1'b1;
        C = 1'b0;
        D = 1'b0;
        #3
        A = 1'b1;
        B = 1'b1;
        C = 1'b0;
        D = 1'b0;
        #3

    //C = 1 D = 0
        A = 1'b0;
        B = 1'b0;
        C = 1'b1;
        D = 1'b0;
        #3
        A = 1'b1;
        B = 1'b0;
        C = 1'b1;
        D = 1'b0;
        #3
        A = 1'b0;
        B = 1'b1;
        C = 1'b1;
        D = 1'b0;
        #3
        A = 1'b1;
        B = 1'b1;
        C = 1'b1;
        D = 1'b0;
        #3

    // C= 0 , D = 1
        A = 1'b0;
        B = 1'b0;
        C = 1'b0;
        D = 1'b1;
        #3
        A = 1'b1;
        B = 1'b0;
        C = 1'b0;
        D = 1'b1;
        #3
        A = 1'b0;
        B = 1'b1;
        C = 1'b0;
        D = 1'b1;
        #3
        A = 1'b1;
        B = 1'b1;
        C = 1'b0;
        D = 1'b1;
        #3

    //C= 1  D = 1
        A = 1'b0;
        B = 1'b0;
        C = 1'b1;
        D = 1'b1;
        #3
        A = 1'b1;
        B = 1'b0;
        C = 1'b1;
        D = 1'b1;
        #3
        A = 1'b0;
        B = 1'b1;
        C = 1'b1;
        D = 1'b1;
        #3
        A = 1'b1;
        B = 1'b1;
        C = 1'b1;
        D = 1'b1;
        #3

        //Limpieza
        A=1'b0;
        B=1'b0;
        C=1'b0;
        D=1'b0;
    end
endmodule
