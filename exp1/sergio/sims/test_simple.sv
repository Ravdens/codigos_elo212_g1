`timescale 1ns / 1ps

module test_simple();

    logic [3:0] BCD_in;
    logic fib;
    
    fib_rec DUT(
        .BCD_in (BCD_in),
        .fib (fib)
    );
    
    initial 
    begin
        BCD_in=4'b0000;
        #3
        BCD_in=4'b0001;
        #3
        BCD_in=4'b0010;
        #3
        BCD_in=4'b0011;
        #3
        BCD_in=4'b0100;
        #3
        BCD_in=4'b0101;
        #3
        BCD_in=4'b0110;
        #3
        BCD_in=4'b0111;
        #3        
        BCD_in=4'b1000;
        #3
        BCD_in=4'b1001;
        #3
        BCD_in=4'b1010;
        #3
        BCD_in=4'b1011;
        #3
        BCD_in=4'b1100;
        #3
        BCD_in=4'b1101;
        #3
        BCD_in=4'b1110;
        #3
        BCD_in=4'b1111;
        #3
        BCD_in=4'b0000;
    end
endmodule