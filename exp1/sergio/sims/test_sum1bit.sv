`timescale 1ns / 1ps

module test_sum1bit();
    logic A,B,Cin;
    logic S,Cout;
    
    sumador1bit DUT(
        .A (A),
        .B (B),
        .Cin (Cin),
        .S (S),
        .Cout (Cout)
    );
    initial begin
        A = 1'b0;
        B = 1'b0;
        Cin = 1'b0;
        #3
        A = 1'b1;
        B = 1'b0;
        Cin = 1'b0;
        #3
        A = 1'b0;
        B = 1'b1;
        Cin = 1'b0;
        #3
        A = 1'b1;
        B = 1'b1;
        Cin = 1'b0;
        #3
        A = 1'b0;
        B = 1'b0;
        Cin = 1'b1;
        #3
        A = 1'b1;
        B = 1'b0;
        Cin = 1'b1;
        #3
        A = 1'b0;
        B = 1'b1;
        Cin = 1'b1;
        #3
        A = 1'b1;
        B = 1'b1;
        Cin = 1'b1;
        #3
        A = 1'b0;
        B = 1'b0;
        Cin = 1'b0;
    end
endmodule
