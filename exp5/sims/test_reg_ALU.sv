`timescale 1ns / 1ps

//    input logic             load_A,load_B,load_Op,updateRes, clk, reset,
//    input logic  [N-1:0]    data_in,
//    output logic [4:0]      LEDs,
//    output logic [6:0]      Segments,
//    output logic [7:0]      Anodes

module test_reg_ALU();
    localparam N=32;
    logic             load_A,load_B,load_Op,updateRes, clk, reset;
    logic  [N-1:0]    data_in;
    logic [4:0]      LEDs;
    logic [6:0]      Segments;
    logic [7:0]      Anodes;
    
    reg_ALU #(.N(N)) DUT (
        .load_A(load_A),
        .load_B(load_B),
        .load_Op(load_Op),
        .updateRes(updateRes),
        .clk(clk),
        .reset(reset),
        .data_in(data_in),
        .LEDs(LEDs),
        .Segments(Segments),
        .Anodes(Anodes)
    );
    
    always #1 clk = ~clk;
    
    initial begin
        load_A=0;
        load_B=0;
        load_Op=0;
        updateRes=0;
        clk=0;
        reset=0;
        data_in=0;
        #2
        reset=1;
        #6
        reset=0;
        #6
        data_in=4;
        #4
        load_A=1;
        #4
        load_A=0;
        #4
        data_in=-5;
        #4
        load_B=1;
        #4
        load_B=0;
        #4
        data_in=1'd1;
        #4
        load_Op=1;
        #4
        load_Op=0;
        #4
        updateRes=1;
        #4
        updateRes=0;
    end
    
    
endmodule
