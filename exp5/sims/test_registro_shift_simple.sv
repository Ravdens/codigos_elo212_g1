`timescale 1ns / 1ps

module test_registro_shift_simple();
    logic clock, reset, enable, shift_in, shift_out;
    registro_shift_simple DUT(
        .clock(clock),
        .reset(reset),
        .enable(enable),
        .shift_in(shift_in),
        .shift_out(shift_out)
    );
    always #1 clock = ~ clock;
    initial begin
        clock=0;
        reset=0;
        enable=0;
        shift_in=0;
        #3
        reset=1;
        #3
        reset=0;
        #3
        shift_in=1;
        #3
        enable=1;
        #3
        shift_in=0;
        #50
        shift_in=0;
        #50
        enable=0;
    end
endmodule
