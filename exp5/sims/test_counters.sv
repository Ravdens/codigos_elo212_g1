`timescale 1ns / 1ps


module test_counters();
    logic clock, reset;
    logic [7:0] out1,out2,out3;
    
    counter1 #(.N(8)) d1 (
        .clock(clock),
        .reset(reset),
        .counter(out1)
    );
    
    counter2 #(.N(8)) d2 (
        .clock(clock),
        .reset(reset),
        .counter(out2)
    );    
    
    counter3 #(.N(8)) d3 (
        .clock(clock),
        .reset(reset),
        .counter(out3)
    );
    
    always #1 clock = ~clock;
    initial begin
        clock = 0;
        reset = 0;
        #2
        reset = 1;
        #6
        reset = 0;
    end
endmodule
