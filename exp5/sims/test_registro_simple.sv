`timescale 1ns / 1ps

module test_registro_simple();
    logic in, out, reset, clock, load;
    registro_simple DUT(
        .in(in),
        .out(out),
        .reset(reset),
        .clock(clock),
        .load(load)
    );
    always #1 clock = ~clock;
    initial begin
        in = 0;
        reset=0;
        clock=0;
        load=0;
        #3
        reset = 1;
        #3
        reset = 0;
        #5
        in = 1;
        #5
        load = 1;
        #5
        load = 0;
        #5
        in = 0;
        #5
        load = 1;
        #5
        in=0;
        load=0;
    end
endmodule
