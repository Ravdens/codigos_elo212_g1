`timescale 1ns / 1ps

module counter_Nbit
# (parameter N=5)
    (
    input logic clk , reset,
    output logic [N-1:0] count
    );
    always_ff @(posedge clk) 
    begin
        if(reset)
            count <= 0;
        else
            count <= count+1;       
    end
endmodule