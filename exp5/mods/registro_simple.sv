`timescale 1ns / 1ps

module registro_simple(
    input logic in, clock, reset, load,
    output logic out
    );
    logic next_out;
    always_ff@(posedge clock) begin
        out <= next_out;
    end
    always_comb begin
        if(reset)
            next_out = 0;
        else
            begin
            if(load)
                next_out=in;
            else
                next_out=out;
            end
    end
endmodule
