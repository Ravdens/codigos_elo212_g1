`timescale 1ns / 1ps

module registro_shift_simple(
    input logic clock, shift_in, reset, enable,
    output logic shift_out
    );
    logic [31:0] shift_reg;
    logic [31:0] next_shift_reg;
    assign shift_out = shift_reg[31];
    always_ff@(posedge clock) begin 
        shift_reg <= next_shift_reg;
    end
    always_comb begin
        if(reset)
            next_shift_reg = 0;
        else begin
            if(enable)
                next_shift_reg = {shift_reg[30:0],shift_in};
            else
                next_shift_reg = shift_reg;
        end
    end
    
endmodule
